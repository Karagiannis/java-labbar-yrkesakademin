/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package lab4xml;

import java.io.File;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import dom_package.*;

/**
 *
 * @author Lasse
 */
public class Lab4XML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try{
                File fXmlFile = new File("C:\\xml\\kommun.xml");
                //File fXmlFile = new File("c:\\LOG\\books.xml");
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();
                
                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                NodeList nList = doc.getChildNodes();
                Element e =  doc.getDocumentElement();
                ArrayList<Element> elemArray = DomUtil.getChildElements(nList);
                System.out.println(e.getNodeName() + "  [OPEN]");
                    for (int i = 0; i < nList.getLength(); i++)
                    {
                        if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                        DomUtil.PrintDOMElemtTreeLab1Extra(nList.item(i), 1);
                    }
                System.out.println(e.getNodeName() + "  [CLOSE]");
                
                
        }catch (Exception e) 
        {
             System.out.println("Exception thrown  now :" + e);
        } 
    
    }
    
}
