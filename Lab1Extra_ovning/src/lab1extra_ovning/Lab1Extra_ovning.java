/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package lab1extra_ovning;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;

/**
 *
 * @author Lasse
 */
public class Lab1Extra_ovning {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try 
        {
               
                // läsa XML filen
                File fXmlFile = new File("C:\\xml\\personal.xml");
                // skapa Documetbuilderfactory
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                // skapa documetbilder
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                // läsa hela XML filen 
                Document doc = dBuilder.parse(fXmlFile);
                // normalizera document informationer		
                doc.getDocumentElement().normalize();
                // Visa Root element
                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                // läsa NodeList		
                NodeList nList = doc.getElementsByTagName("personal");
                            
                           for (int i = 0; i < nList.getLength(); i++){
                               PrintNodeElement(nList.item(i)); 
                           }
                                             
                    
         } catch (Exception e) {
         System.out.println("Exception thrown  :" + e);
         } 
        
        
    }//end main
    public static void PrintNodeElement(Node n)
    {
        
        switch(n.getNodeType()) 
        {
            case Node.ELEMENT_NODE:
                Element e = (Element) n;
                System.out.println(e.getTagName());
                listAllAttributes(e);
                e.getTextContent();
                if (e.hasChildNodes())
                {
                    NodeList list = e.getChildNodes();
                    for (int i = 0; i < list.getLength(); i++)
                        PrintNodeElement(list.item(i));
                }
                break;
            case Node.ATTRIBUTE_NODE:
                
                System.out.println("Attribute node: "+ n.getNodeValue());
                break;
            case Node.TEXT_NODE:
                System.out.println("Text node: " + n.getNodeValue());
                break;                      
        }
        
    
    }
    public static void listAllAttributes(Element element) 
    {
	        System.out.println("List attributes for node: " + element.getNodeName());
	         // get a map containing the attributes of this node
	        NamedNodeMap attributes = element.getAttributes();
	        // get the number of nodes in this map
	        int numAttrs = attributes.getLength(); 
	        for (int i = 0; i < numAttrs; i++) 
                {
	            Attr attr = (Attr) attributes.item(i);
                    String attrName = attr.getNodeName();
	            String attrValue = attr.getNodeValue();
	            System.out.println("Found attribute: " + attrName + " with value: " + attrValue);
	        }
    }
}
