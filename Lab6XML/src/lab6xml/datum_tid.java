/*
 * YrkesHögskola Östursund
 * XML
 * Laboration : Projekt
 * Lärare: Fredrik Håkansson
 * @Author: Ahmad Setoodeh
 * Termin: HT-2015
 * Datum: 2015-12-10
 * @version : 1.0
 * Filnamn : datum_tid.java

 */
package lab6xml;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 *
 * @author John
 */
public class datum_tid {
    String Datum;
    String Tid;
    // konstrator
    public datum_tid(){
        DateFormat datum_Format = new SimpleDateFormat("yyyy/MM/dd");
        Date datum = new Date();
        this.Datum =datum_Format.format(datum);
        
        DateFormat tid_Format = new SimpleDateFormat("HH:mm:ss");
        Date tid = new Date();
        this.Tid= tid_Format.format(tid); 
    }
    // sätta datum
     public void setDatum() {
        DateFormat datum_Format = new SimpleDateFormat("yyyy/MM/dd");
        Date datum = new Date();
        this.Datum =datum_Format.format(datum);
         
    }
    //Sätta tid 
    public void setTid() {
        DateFormat tid_Format = new SimpleDateFormat("HH:mm:ss");
        Date tid = new Date();
        this.Tid= tid_Format.format(tid);
    }
    // retunera datum
    public String GetDatum(){
        return Datum;
    }
    // retunera tid
    public String GetTid(){
        return Tid;
    }
    
    
}
