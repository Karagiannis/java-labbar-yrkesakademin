/*
 * YrkesHögskola Östursund
 * XML
 * Laboration : Projekt
 * Lärare: Fredrik Håkansson,Ahmad Setoodeh
 * @Author:                    <--- skriva ditt namn
 * Email:                      <--- skriva ditt mejl adress
 * Termin: HT-2015             
 * Datum: 2015-12-00           <--- skriva datum
 * @version : 0.0              <--- skriva version
 * Filnamn : Lab6XML.java
 * När det skapas ny Projekt kopiera datum_tid.java till 
 * C:\..\Lab6XML\src\eleve_projekt(ditt package name) 
 * eller skapa ny class datum_tid och sedan kopier innehåller av fil till ditt ny class 
 * Glöm inte skriva kommentar för varje metoder.
 */
package lab6xml;
import Lab6Support.SupportLab6;
import java.io.*;
import java.util.Date;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.w3c.dom.*;
import javax.xml.parsers.*;
import javax.xml.transform.*;
import javax.xml.transform.stream.*;
import javax.xml.transform.dom.DOMSource;
import org.xml.sax.SAXException;
import dom_package.*;
/**
 * Kompeletera java koder som kan köra programmet för att visa information
 * Aktivera varje metode som kan skriva java kod
 *------------------------------------------------------------------
 *  Skriva koden som skapa information
 *------------------------------------------------------------------
 *  
 */
public class Lab6XML {

     // deklarera variabel
    // Namn för mata in information
    public static String[] info_mata_in = {"ID nummer", "Artikel nummer", "Artikel namn", "Kategori"
                                          , "Pris", "Antal", "Antal Kvar"
                                          , "Barcod nummer", "Lager nummer", "Lager plats"}; 
    // element namn
    public static String[] element_info = {"id", "artikelnr", "artikel_namn", "kategori"
                                          , "pris", "antal", "antal_kvar"
                                          , "barcod_nr", "lager_nr", "lager_plats", "tid", "datum"};
    
    private static String[] element_nya = new String[12];
    // Fillar namn och 
    private static String paths="C:\\xml\\projekts\\", _xml="artiklar.xml",_xsl="artiklar.xsl",_html="artiklar.html";
    // kontrollera att vilka information ska mata in i Nya_tag_XML eller Redigera_tag_XML 
    static int kontroll=0;
    static DocumentBuilderFactory dbFactory = null;
    // skapa documetbilder
    static DocumentBuilder dBuilder = null;
    static Document doc_artiklar=null;
    // main metod
    public static void main(String[] args) {
        try {
            // kontrollera om map finns inte skapa den
            CDIN(paths);
            int nummer = -1;

            do {
                // visa meny
                Meny();
                // ta nummer från tangantboard
                BufferedReader read_key = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Välja ett nummer :");
                String enter = read_key.readLine();
                
                if(!enter.isEmpty())
                {
                nummer = Integer.parseInt(enter);
                switch (nummer) {
                    case 0:
                        System.out.println("Programmet är avslut");
                        System.exit(nummer);

                        break;
                    case 1:
                        Skapa_XML();
                        break;
                    case 2:
                        Lagga_Nya_Element_XML();
                        break;
                    case 3:
                        Visa_XML();
                        break;
                    case 4:
                        kontroll=5;
                        Nya_Node_XML();
                        break;
                    case 5:
                        Tabort_Element_XML();
                        break;
                    case 6:
                         kontroll=6;
                         Redigera_Element_XML();
                        break;
                    case 7:
                        XML_Till_XSLT();
                        break;
                   
                }
                }else{System.out.println("Var vänlig välja ett nummer.");     }
            } while (nummer != 0);
            
        } catch (IOException | NumberFormatException e) {
           System.out.println("Fel :" + e); 
          
        } catch (TransformerException ex) {
            Logger.getLogger(Lab6XML.class.getName()).log(Level.SEVERE, null, ex);
        }
        return;
    }

    // huvud meny
    public static void Meny() {
        System.out.println("----------------------------------");
        System.out.println("1-Skapa XML filen");
        System.out.println("2-Lägga till nya element i XML filen");
        System.out.println("3-Visa  XML filen");
        System.out.println("4-Lägga ny noden i element i XML filen");
        System.out.println("5-Ta bort element från XML filen");
        System.out.println("6-Redigera information i XML filen");
        System.out.println("7-Transformera XML till HTML");
        System.out.println("0-Avslut");
        System.out.println("----------------------------------");
    }
    
    // 1 metoden som skapa XML filen
    public static void Skapa_XML() throws TransformerException
    {
          System.out.println("1-Skapa XML file");
        try{
        // kontrollera om det finns XML filen
        File File_Exist = new File(paths+_xml);
              
              if(File_Exist.exists() && !File_Exist.isDirectory()) 
              { 
                 System.out.println("Filen Artiklar.xml finns redan i "+File_Exist);
                 return;
              }
        // skapa Documetbuilderfactory
        dbFactory = DocumentBuilderFactory.newInstance();
        // skapa documetbilder
        dBuilder = dbFactory.newDocumentBuilder();
        // läsa hela XML filen 
        doc_artiklar = dBuilder.newDocument();
        
        
         Element rootElement = doc_artiklar.createElement("Artiklar");
         doc_artiklar.appendChild(rootElement);
         Element artikel = doc_artiklar.createElement("Artikel");
        rootElement.appendChild(artikel);
        
//------------------------------------------------------------------
//  Skriva koden som skapa information
          Get_information();
         // root element
         
         for (int i = 0; i < element_nya.length; i++ )
         {
             if(i == 0)
             {
                 // write the content into xml file
		Attr attr = doc_artiklar.createAttribute("id");
		attr.setValue(element_nya[i]);
		artikel.setAttributeNode(attr);
             }
             else
             {
                Element element = doc_artiklar.createElement(element_info[i]);
                System.out.println(element_info[i]);
                Node text = doc_artiklar.createTextNode(element_nya[i]);
                System.out.println(element_nya[i]);
                element.appendChild(text);
                artikel.appendChild(element);
             }
         }
        

         // write the content into xml file
         TransformerFactory transformerFactory = TransformerFactory.newInstance();
         Transformer transformer = transformerFactory.newTransformer();
         
         transformer.setOutputProperty(OutputKeys.INDENT, "yes");
         transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");
         
         DOMSource source = new DOMSource(doc_artiklar);
         StreamResult result = new StreamResult(new File(paths+_xml));
         transformer.transform(source, result);
         // Output to console for testing
         StreamResult consoleResult = new StreamResult(System.out);
         transformer.transform(source, consoleResult);
//------------------------------------------------------------------
       
        } catch(ParserConfigurationException | TransformerException PCEe) 
        {   
            System.out.println("Fel :" + PCEe);
        }
//        catch(TransformerConfigurationException TCEe) { 
//            System.out.println("Fel :" + TCEe);
        //     }
   
        
    }
    
   // 2 metoden som lägga nya element till xml fillen
    public static void Lagga_Nya_Element_XML() throws TransformerException {
        System.out.println("2-Lägga till nya element i XML filen");
        
        try{
        // skapa Documetbuilderfactory
        dbFactory = DocumentBuilderFactory.newInstance();
        // skapa documetbilder
        dBuilder = dbFactory.newDocumentBuilder();
        // läsa hela XML filen 
        doc_artiklar = dBuilder.parse(new FileInputStream(new File(paths+_xml)));
   
      //  Document doc_artiklar =DocumentBuilderFactory.newInstance().newDocumentBuilder().parse(paths);
        Element root = doc_artiklar.getDocumentElement();
        // lägga nya information genom tangatboard
        
        Element artikel = doc_artiklar.createElement("Artikel");
        root.appendChild(artikel);
         Get_information();

//  Skriv koden som lägger till nya element 
           
         SupportLab6.AppendArticleToArticles(doc_artiklar, root,element_info, element_nya );
          // write the content into xml file
   
         SupportLab6.WriteDOMtoXMLFile(doc_artiklar,paths+ _xml);
            
       } catch(ParserConfigurationException | SAXException | IOException PCEe) {   
            System.out.println("Fel :" + PCEe);
       } 

    }
    
    // 3 metoden som visa alla information från xml filen
    public static void Visa_XML() throws FileNotFoundException
    {
        System.out.println("3-Visa XML file");
      // läsa XML filen
       
      
       
        BufferedReader br = new BufferedReader(new FileReader("C:\\xml\\projekts\\artiklar.xml")); 
        String s; 
        try {
            while((s = br.readLine()) != null) {
                System.out.println(s); 
            }
        } catch (IOException ex) {
            Logger.getLogger(Lab6XML.class.getName()).log(Level.SEVERE, null, ex);
        }
    
        
        
   }
          
    
    
    // 4 metoden som lägga nya node på artikel element
    public static void Nya_Node_XML() {
        System.out.println("4-Lägga ny node i element i XML filen");
           try
	   {
             // skapa Documetbuilderfactory
            dbFactory = DocumentBuilderFactory.newInstance();
            // skapa documetbilder
            dBuilder = dbFactory.newDocumentBuilder();
            // läsa hela XML filen 
            doc_artiklar = dBuilder.parse(new FileInputStream(new File("C:\\xml\\projekts\\artiklar.xml")));

	    //root 
	    System.out.println(doc_artiklar.getDocumentElement().getNodeName());
	    //ny elemnts
	    NodeList nodeList = doc_artiklar.getElementsByTagName("Artikel");
	    System.out.println("Max information är :"+nodeList.getLength());
            int find=0;
            // deklarera Buffer för mata in information
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Vilken node nummer ?:");
            String eleman_num = br.readLine();
            find=Integer.parseInt(eleman_num)-1;

            if(find <= nodeList.getLength())
            {
////------------------------------------------------------------------
////  Skriva koden som lägga nya tag 
                Node n = nodeList.item(find);           
                System.out.println("Skrivnod namnet   :");
                // mata in nya information            
                String newNode = br.readLine();
                System.out.println("Skriv värdet   :");
                String value = br.readLine();
                
                
                
                Element element = doc_artiklar.createElement(newNode);

                Node text = doc_artiklar.createTextNode(value);
                element.appendChild(text);
                n.appendChild(element);
                    
                
         // write the content into xml file
   
         SupportLab6.WriteDOMtoXMLFile(doc_artiklar,paths+ _xml);
                
////------------------------------------------------------------------
	       
            }else{
                System.out.println("OBS !!! \nNummer är mer om antal informationer.");
            }  
	   }
	   catch(Exception e)
	   {
		   System.out.println("Exception :"+e);
	   }
    }
   
    // 5 metoden som radera bort ett element från artiklar.xml
    public static void Tabort_Element_XML() 
    {
        System.out.println("5-Ta bort element från XML filen");
        try
	   {
             // skapa Documetbuilderfactory
            dbFactory = DocumentBuilderFactory.newInstance();
            // skapa documetbilder
            dBuilder = dbFactory.newDocumentBuilder();
            // läsa hela XML filen 
            doc_artiklar = dBuilder.parse(new FileInputStream(new File("C:\\xml\\projekts\\artiklar.xml")));

	    //root 
	    System.out.println(doc_artiklar.getDocumentElement().getNodeName());
	    //ny elemnts
	    NodeList nodeList = doc_artiklar.getElementsByTagName("Artikel");
	    System.out.println("Max information är :"+nodeList.getLength());
            int find=0;
            // deklarera Buffer för mata in information
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Vilken node nummer ?:");
            String eleman_num = br.readLine();
            find=Integer.parseInt(eleman_num)-1;

            nodeList.item(find).getParentNode().removeChild(nodeList.item(find));
            
          
                    
                
         // write the content into xml file  
         SupportLab6.WriteDOMtoXMLFile(doc_artiklar,paths+ _xml);
            
    }catch(Exception e)
	   {
		   System.out.println("Exception :"+e);
	   }
                
}
       
    // 6 metoden som redigera element från artiklar.xml
    public static void Redigera_Element_XML() {
        System.out.println("6-Redigera information i XML filen");
           try
	   {
             // skapa Documetbuilderfactory
            dbFactory = DocumentBuilderFactory.newInstance();
            // skapa documetbilder
            dBuilder = dbFactory.newDocumentBuilder();
            // läsa hela XML filen 
            doc_artiklar = dBuilder.parse(new FileInputStream(new File(paths+_xml)));

	    //root 
	    System.out.println(doc_artiklar.getDocumentElement().getNodeName());
            
	    //ny elemnts
            NodeList Find_nodeList = doc_artiklar.getElementsByTagName("Artikel");
            System.out.println("Max information är :"+Find_nodeList.getLength());
      
            int find=0;
            // deklarera Buffer för mata in information
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            System.out.print("Vilken node nummer ?:");
            String eleman_num = br.readLine();
            find=Integer.parseInt(eleman_num)-1;
            Get_information();
            if(find <= Find_nodeList.getLength())
            {
//------------------------------------------------------------------
//  Skriva koden som redigera xml elementar
                
            Node parent = Find_nodeList.item(find).getParentNode();
            Element nextSiblingElement = DomUtil.getNearestSiblingElement((Element)Find_nodeList.item(find));
            Find_nodeList.item(find).getParentNode().removeChild(Find_nodeList.item(find));
            Element root = doc_artiklar.getDocumentElement();
            
            SupportLab6.InsertArticleBeforeSiblingInArticles(doc_artiklar, root,
            element_info, element_nya, (Node) nextSiblingElement );
         

           
         // write the content into xml file
   
         SupportLab6.WriteDOMtoXMLFile(doc_artiklar,paths+ _xml);
           
           
//------------------------------------------------------------------               
	       
            }else{
                System.out.println("OBS !!! \nNummer är mer om antal informationer.");
            }  
	   }
	   catch(ParserConfigurationException | SAXException | IOException | NumberFormatException | DOMException | TransformerException e)
	   {
		   System.out.println("Exception :"+e);
	   }
    }
    
    // 7 metoden som transformera xml filen till html
    public static void XML_Till_XSLT() {
             System.out.println("7-Transformera XML till HTML");
             File File_Exist = new File(paths+_xsl);
             if(!File_Exist.exists() && !File_Exist.isDirectory())
             { 
                 System.out.println("Filen artiklar.xsl finns inte  i "+paths);
                 return;
             }
             try {
                 TransformerFactory tFactory = TransformerFactory.newInstance();
                 Transformer transformer = tFactory.newTransformer(new javax.xml.transform.stream.StreamSource(paths+_xsl));
                 transformer.transform(new javax.xml.transform.stream.StreamSource(paths+_xml),
                         new javax.xml.transform.stream.StreamResult( new FileOutputStream(paths+_html)));
             }
             catch (FileNotFoundException | TransformerException e) {
             }
             System.out.println("Det är skapat HTML filen :" + _html); 
	        
    }
    
    // metode som mata in informationer 
    public static void Get_information ()  
    {
        try
        {
            BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
            int get_in=0;
            if(kontroll==6)
            {
                get_in=2;
            }
            for (int enter = get_in; enter < info_mata_in.length; enter++) 
            {
                System.out.print("Skriva " + info_mata_in[enter] + " :");
                // mata in nya information            
                element_nya[enter] = br.readLine();
                // Visa meddelandet om man mata in  tomma informationer
                if (element_nya[enter].isEmpty()&& kontroll==5) 
                {
                    System.out.println(" Skriva rätt värde för "+ info_mata_in[enter]+" !!!.");
                    enter--;
                }
            }
            datum_tid datums = new datum_tid();
            element_nya[11] = datums.GetDatum();
            element_nya[10] = datums.GetTid();
            System.out.println("Datum är :" + element_nya[11]);
            System.out.println("Tid är :" + element_nya[10]);
        }catch(IOException e)
        {
            System.out.println(e);
        }
       
    }
    // kontrollera map
    private static void CDIN(String MapNamn)
    {
    File theDir = new File(MapNamn);
   // skapa map 
    if (!theDir.exists())
    {
    theDir.mkdir();
    }
}
    
}

