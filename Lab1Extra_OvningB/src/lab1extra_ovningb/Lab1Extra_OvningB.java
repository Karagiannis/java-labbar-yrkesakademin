/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package lab1extra_ovningb;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.NodeList;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import java.util.*;
import org.w3c.dom.Attr;
import org.w3c.dom.NamedNodeMap;
import dom_package.*;

/**
 *
 * @author Lasse
 */
public class Lab1Extra_OvningB {
    
     

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) 
            
    {
        try{
                File fXmlFile = new File("C:\\xml\\personal.xml");
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                Document doc = dBuilder.parse(fXmlFile);
                doc.getDocumentElement().normalize();
                
                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                NodeList nList = doc.getChildNodes();
                Element e =  doc.getDocumentElement();
                ArrayList<Element> elemArray = DomUtil.getChildElements(nList);
                System.out.println(e.getNodeName() + "  [OPEN]");
                    for (int i = 0; i < nList.getLength(); i++)
                    {
                        if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                        DomUtil.PrintDOMElemtTreeLab1Extra(nList.item(i), 1);
                    }
                System.out.println(e.getNodeName() + "  [CLOSE]");
                System.out.println("*************NEW ANALYSIS***************");
                int[] content = {0,0,0,0};
                e =  doc.getDocumentElement();
                DomUtil.AnalyzeDomTreeLab1Extra(e, content, 0);
                System.out.println("*************NEW ANALYSIS2***************");
                DomUtil.AnalyzeDomTreeLab1ExtraSecond(e);
                
        }catch (Exception e) 
        {
             System.out.println("Exception thrown  now :" + e);
        } 
    
    }
}