/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package lab2xml;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Lasse
 */
public class Lab2XML {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
         try {
              File f = new File("C:\\xml\\bil.xml");
              
              if(f.exists() && !f.isDirectory()) 
              { 
                  f.delete();
              }
		DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
		DocumentBuilder docBuilder = docFactory.newDocumentBuilder();

		// root elements
		Document doc = docBuilder.newDocument();
		Element rootElement = doc.createElement("foretag");
		doc.appendChild(rootElement);

		// information
		Element info = doc.createElement("information");
		rootElement.appendChild(info);

		// Attribute on information tag
		Attr attr = doc.createAttribute("Orgnummer");
		attr.setValue("556689-3292");
		info.setAttributeNode(attr);
		
		// foretagsnamn
		Element foretagsNamn = doc.createElement("foretagsnamn");
		foretagsNamn.appendChild(doc.createTextNode("Postnummer service Norden AB"));
		info.appendChild(foretagsNamn);

		// telefon
		Element telefon = doc.createElement("telefon");
		telefon.appendChild(doc.createTextNode("08-21023970"));
		info.appendChild(telefon);

		// adress
		Element adress = doc.createElement("adress");
		adress.appendChild(doc.createTextNode("Box 30178"));
		info.appendChild(adress);

		// post nr
		Element postnr = doc.createElement("postnr");
		postnr.appendChild(doc.createTextNode("104 25"));
		info.appendChild(postnr);
                
               // stad
		Element stad = doc.createElement("stad");
		stad.appendChild(doc.createTextNode("Stockholm"));
		info.appendChild(stad);
                
                //branscher
                Element branscher = doc.createElement("branscher");
		 branscher.appendChild(doc.createTextNode("Adressregister"));
		info.appendChild(branscher);

                
		// skriva information i xml filen
		TransformerFactory transformerFactory = TransformerFactory.newInstance();
		Transformer transformer = transformerFactory.newTransformer();
                // skapa objetkt som kan spara information i xml filen
		DOMSource source = new DOMSource(doc);
		StreamResult result = new StreamResult(new File("C:\\XML\\foretag.xml"));

		transformer.transform(source, result);

		System.out.println("File sparas!");

	  } catch (Exception e) {  System.out.println("Exception thrown  :" + e);   }
    }
    
}
