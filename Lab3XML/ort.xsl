<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" version="1.0">
 
    <xsl:template match="/">
        <html>
            <head>
                <title>ort.xsl</title>
            </head>
            <body>
                <h2>Orter</h2>
                <table border="1">
                    <tr bgcolor="#9acd32">
                        <th>Id ort</th>
                        <th>Lans kod</th>
                        <th>Entitet</th>
                        <th>Region</th>
                        <th>Ort</th>
                        <th>Antal traffar</th>
                        <th>Datum</th>
                        <th>Tid</th>
                    </tr>
                    <xsl:for-each select="database_ort/table">     
                        <tr>
                            <td><xsl:value-of select="id_ort"/></td>
                            <td><xsl:value-of select="ort_lan_code"/></td>
                            <td><xsl:value-of select="ort_alf_namn"/></td>
                            <td><xsl:value-of select="ort_lan_namn"/></td>
                            <td><xsl:value-of select="ort_namn"/></td>  
                                   
                            <xsl:choose>
                                <xsl:when test="ort_traffa &gt; 20">
                                    <td bgcolor="#ff00ff">
                                    <xsl:value-of select="ort_traffa"/></td>
                                </xsl:when>
                                <xsl:otherwise>
                                    <td><xsl:value-of select="ort_traffa"/></td>
                                </xsl:otherwise>
                            </xsl:choose>

                            <td><xsl:value-of select="ort_datum"/></td>
                            <td><xsl:value-of select="ort_tid"/></td>
                        </tr> 
                    </xsl:for-each>
                </table>        
            </body>
        </html>
    </xsl:template>
</xsl:stylesheet>
