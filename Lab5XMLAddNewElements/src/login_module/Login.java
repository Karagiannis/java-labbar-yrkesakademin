/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package login_module;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 *
 * @author Lasse
 */
public class Login {
    
      private String login = "elev";
      private String password = "abc123";
      private String loginAttempt="";
      private String passwordAttempt="";
    
     public Login() throws IOException
    {
     
      this.TakeCareOfLogin();
       
      
    }
    
    public boolean CheckLogInAndPassword()
    {
        if (this.loginAttempt.equals(this.login) &&
                this.passwordAttempt.equals(this.password))
            return true;
        else
            return false;
    }
    public  void TakeCareOfLogin() throws IOException
    {
        System.out.println("Login Name: ");
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        this.loginAttempt = br.readLine();
        System.out.println("Password: ");
        this.passwordAttempt = br.readLine();
    
    }
}
