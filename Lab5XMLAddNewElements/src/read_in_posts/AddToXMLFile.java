/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package read_in_posts;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

/**
 *
 * @author Lasse
 */
public class AddToXMLFile {
    public static boolean CheckIfIdExists( String id)
    {
        return false;
    }
    public static Element TakeCareOfInputPost(Document document) throws IOException
    {    
        String[] Book_List= {"id","author","title","genre","price","publish_date","description"};
        String[] Nya_book = new String[8];
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
       
        Element newBook = document.createElement("book");

        Attr id_Attr;
        int i = 0;
        while( i < 7)
           {
                switch (i)
                {
                    case 0:
                    {
                        String id = TakeCareOfInput("id");
                        boolean idExists= AddToXMLFile.CheckIfIdExists(id);
                        if (!idExists)
                        {
                            newBook = document.createElement("book");
                            // sätta attribut till book element
                            id_Attr = document.createAttribute("id");
                            id_Attr.setValue(id);
                            newBook.setAttributeNode(id_Attr);
                            Nya_book[i] = id;
                            i++;
                        }
                        else
                        {
                            System.out.println("Id Exists");
                        }
                        break;
                    }
                    case 1:
                    {
                        String title = TakeCareOfInput("Title");
                        Nya_book[i] = title;
                        i++;
                        break;
                    }
                    case 2: 
                    {
                        String author = TakeCareOfInput("Author");
                        Nya_book[i]=author;
                        i++;
                    break;
                    }
                    case 3:
                    {
                        String genre = TakeCareOfInput("Genre");
                        Nya_book[i] = genre;
                        i++;
                        break;
                    }
                    case 4:
                    {
                        String price = TakeCareOfInput("Price");
                        Nya_book[i] = price;
                        i++;
                        break;
                    }
                    case 5:
                    {
                        String pubDate = TakeCareOfInput("Publish date"); 
                        Nya_book[i]=pubDate;
                        i++;
                        break;
                    }
                    case 6:
                    {
                        String description = TakeCareOfInput("Description");
                        Nya_book[i] = description;
                        for(int s=1;s<Book_List.length;s++)
                        {
                            // läsa varje element värde från array
                             Element tagName = document.createElement(Book_List[s]);
                            tagName.appendChild(document.createTextNode(Nya_book[s]));
                            newBook.appendChild(tagName); 
                        }
                        i ++;
                        break;
                    }
                    default: break;
                }
            }
        return newBook;
    }
    private static String TakeCareOfInput(String tag) throws IOException
    {
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        System.out.println(tag + ": ");
                    String input = br.readLine();
                    if (input.equals(":wq"))
                    {
                        System.out.println("Shutting down due to user request :wq");
                        System.exit(0);
                        return null;
                    }
                    else
                    {
                        return input;
                    }   
    } 
}
