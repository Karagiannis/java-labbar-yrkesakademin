/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package lab5xml;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import javax.xml.parsers.*;
import org.w3c.dom.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import login_module.Login;
import read_in_posts.AddToXMLFile;
/**
 *
 * @author Lasse
 */
public class Lab5XML {

   
    public static void main(String[] args) throws Exception {
           
        DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();      
        Document document = documentBuilder.parse("..\\Lab5XMLAddNewElements\\xml\\books.xml");
        Element root = document.getDocumentElement();
               
        Login login = new Login();// login:elev pass:abc123
        BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        String str;
         boolean loginSucceeded = login.CheckLogInAndPassword();
         
        if (loginSucceeded)
        {
            do{
                System.out.println("Enter posts, hit any key or close with :wq ");
                str = br.readLine();
                if (str.equals(":wq")) System.exit(0);

                    Element newBookElement = AddToXMLFile.TakeCareOfInputPost(document);
                    root.appendChild(newBookElement);

            }while(!str.equals(":wq"));
 
            DOMSource source = new DOMSource(document);
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer(); 
            StreamResult result = new StreamResult("..\\Lab5XMLAddNewElements\\xml\\books.xml");
            transformer.transform(source, result);
        }   
    } 
}
