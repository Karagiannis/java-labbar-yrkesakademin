/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package htmlsupport;

/**
 *
 * @author Lasse
 */
public class Text extends HTML 
{
    //HTML html;
    String s;
    public Text(String s){
         this.s = s;
    }
    
    @Override
    public String toHTML(){
      return this.s;
    }
}

