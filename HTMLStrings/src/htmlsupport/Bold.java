/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package htmlsupport;

/**
 *
 * @author Lasse
 */
public class Bold extends HTML{
    HTML html;

    public Bold(HTML html){
        this.html = html;
    }

    @Override
    public String toHTML(){
      return "<b>" + html.toHTML() + "</b>";
    }
}
