/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package readenkelxml3;

import java.io.File;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Lasse
 */
public class ReadEnkelXML3 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        try 
        {
               
                // läsa XML filen
                File fXmlFile = new File("C:\\xml\\personal.xml");
                // skapa Documetbuilderfactory
                DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                // skapa documetbilder
                DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                // läsa hela XML filen 
                Document doc = dBuilder.parse(fXmlFile);
                // normalizera document informationer		
                doc.getDocumentElement().normalize();
                // Visa Root element
                System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                // läsa NodeList		
                NodeList nList = doc.getElementsByTagName("personal");

                System.out.println("----------------------------");
                            
                            // läsa noder
                            
                            for (int i = 0; i < nList.getLength(); i ++)
                            {
                                Node nNode = nList.item(i);
//                                System.out.println("**************************");
//                                System.out.print(nNode.getNodeName() + " ");
//                                System.out.print(nNode.getFirstChild().getNodeName() + " ");
//                                System.out.println("**************************");
//                                System.out.println("\nCurrent Element :" + nNode.getNodeName());
//                                
//                                Node[] nodeArrary = nList.getChildNodes();
                                
                                if (nNode.getNodeType() == Node.ELEMENT_NODE) 
                                {
                                        Element eElement = (Element) nNode;
                                        System.out.println("personal id : " + eElement.getAttribute("id"));
                                        System.out.println("Forenam : " + eElement.getElementsByTagName("forenamn").item(0).getTextContent());
                                        System.out.println("Efternamn : " + eElement.getElementsByTagName("efternamn").item(0).getTextContent());
                                        //System.out.println("genre : " + eElement.getElementsByTagName("genre").item(0).getTextContent());
                                        System.out.println("Löm: " + eElement.getElementsByTagName("lon").item(0).getTextContent());
                                        //System.out.println("Publiceringsdatum : " + eElement.getElementsByTagName("publish_date").item(0).getTextContent());
                                       // System.out.println("Beskrivning : " + eElement.getElementsByTagName("description").item(0).getTextContent());
                                }
                            }
                    
         } catch (Exception e) {
         System.out.println("Exception thrown  :" + e);
         } 
    }
    
}
