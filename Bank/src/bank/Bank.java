/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;


import gui.UserGUI;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;

/**
 *
 * @author Lasse
 */
public class Bank{
/**
     * @param args the command line arguments
     */
    // TODO code application logic here
        Prompt prompt = new Prompt();
        ArrayList<BankObject_BankAccount> bankAccounts = new ArrayList<>();
        ArrayList<BankObject_BankUser> bankUsers = new ArrayList<>();
        ArrayList<BankObject_BankLoan> bankLoans = new ArrayList<>();
        public BankEvents_BankLoansToXMLFileHandler events = new BankEvents_BankLoansToXMLFileHandler(new BankEvents_BankUsersToXMLFileHandler
            (new BankEvents_BankAccountsToXMLFileHandler(new BankEventsCmdLine(bankAccounts,bankUsers,bankLoans, prompt),bankAccounts),bankUsers),bankLoans);
        ArrayList<BankCommand_> customerCommands;
        ArrayList<BankCommand_> employeeCommands;
        
        
        public Bank(){
            customerCommands = new ArrayList<>();
            employeeCommands = new ArrayList<>();
        
        }
    
    
//    public static void main(String[] args) throws UnsupportedEncodingException {
//        
//        
//        System.out.println("Nu startar appen i main");
//        Bank library = new Bank();
//        library.run();
//        //Application.launch(FXMLExample.class, args);
//        //UserGUI gui = new UserGUI();
//       
//    }
//    
        
   
    public void start(Stage stage) throws Exception {
        
    }
        
        

    public void run(){
    
     
        while(true)
        {
                int command = events.WelcomeString();
                BankSession_ session;

                switch(command)
                {
                    case 1:
                        session = new BankSession_SuperUserSession(prompt,bankAccounts,bankUsers, bankLoans, events);
                        session.start();
                        break;
                    case 2:
                        session = new BankSession_EmployeeSession(prompt,bankAccounts,bankUsers, bankLoans, events);
                        session.start();
                        break;
                     case 3:
                        session = new BankSession_SuperUserSession(prompt,bankAccounts,bankUsers, bankLoans, events);
                        session.start();
                        break;
                    
                }           
        }   
    }   
public BankSession_ login(String userName, String passWord){
    
         
          
          int i = events.findLoginData(userName, passWord);
                if(i == 1){
                    return new BankSession_SuperUserSession(prompt,bankAccounts,bankUsers, bankLoans, events); 
                }
                else if(i == 2)
                 {
                    return new BankSession_EmployeeSession(prompt,bankAccounts,bankUsers, bankLoans, events);      
                 }       
                
                        
            System.out.println("Something is wrong null is returned");
          return null;
    
    }
}