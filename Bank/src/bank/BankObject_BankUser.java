/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankObject_BankUser extends BankObject_{
    
    public String name;
    public String familyName;
    public int userID;
    public String personNumber;
    public String postAdress;
    public String postNumber;
    public String city;
    public String email;
    public String passWord;
    public String category;
    public ArrayList<BankObject_BankLoan> bankLoans;
    public ArrayList<BankObject_BankLoan> bankAccounts;
    
    public BankObject_BankUser(int userID, String name, String familyName ){
           this.userID = userID;
           this.name = name;
           this.familyName = familyName; 
           bankLoans = new ArrayList<BankObject_BankLoan>();
           bankAccounts = new ArrayList<>();
    }
    public BankObject_BankUser(){}
    
    public BankObject_BankUser getUser(int userID){
        
        if(userID == this.userID)
            return this;
        else
            return null;
    
    }
    public String getUserPassWord(){
        
            return passWord;
    }
    
    
    public void registerALoan(BankObject_BankAccount b){
        BankObject_BankLoan bankLoan = new BankObject_BankLoan(b);
        bankLoans.add(bankLoan);
        System.out.println("Boklånet har registrerats");
    
    }
    public String getName(){
        return name+ " "+familyName;
    }
    public int getID(){
        return userID;
    }
    
    public ArrayList<BankObject_BankLoan> getUserLoans(){
    
        return bankLoans;
    }
    public String getUserData(){
      String data;
        data = String.format("%s userID: %d", getName(),userID);
      return data;
    }
    
    public void unregisterALoan(int bookID){
        
        for(BankObject_BankLoan loan : bankLoans){
            System.out.println("loan.getBookIDFromBookLoan()" + loan.getBookIDFromBookLoan());
            System.out.println("Enterd for-loop");
            if(loan.getBookIDFromBookLoan() == bookID){
                System.out.println("Found book ID " + bookID );
                bankLoans.remove(loan);
                System.out.println("Boklånet har avregistrerats");
                break;
            }
        }
     
    }
    
    
    
    public boolean checkIfBookLoanRegistered(int bookID){
        for(BankObject_BankLoan book : bankLoans){
            if(book.bookID == bookID){
                return true;
            }
        }
        return false;
    
    }
    
}
