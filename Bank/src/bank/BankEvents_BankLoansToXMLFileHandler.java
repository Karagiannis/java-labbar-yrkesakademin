/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

/**
 *
 * @author Lasse
 */
    
import dom_package.DomUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import banksupporxml.BankSupport;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class BankEvents_BankLoansToXMLFileHandler extends BankEventsDecorator{
    
    ArrayList<BankObject_BankLoan> libraryLoans;
    String filePathLibraryLoans = "c:\\LOG\\libraryLoans.xml";

    
    public BankEvents_BankLoansToXMLFileHandler(BankEventsInterface libEvents, ArrayList<BankObject_BankLoan> libraryLoans) {
        
        super(libEvents);
        this.libraryLoans = libraryLoans;
        
        //Transfer data from xml-file to datastructure loans, every time program is started
           loadLoansFromXMLFileToDataStructure();
           
    }

    
           
   
    
    private void loadLoansFromXMLFileToDataStructure(){
    
    
        //Populate datastructure libraryLoans
            try{
                    File fXmlFile = new File(filePathLibraryLoans);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    //NodeList nList = doc.getChildNodes();
                    Element e =  doc.getDocumentElement();
                    //Element first = DomUtil.getFirstElementChild(e);
                    ArrayList<Element> elemArray = DomUtil.getChildElements(e);
                    System.out.println(e.getNodeName() + "  [OPEN]");
                        for (int i = 0; i < elemArray.size(); i++)
                        {
                            System.out.println("Size "+ elemArray.size());
                           
                            BankObject_BankLoan b = new BankObject_BankLoan();
                            //if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                           
                            DomUtil.LibraryLoansFillingDataRoutine(elemArray.get(i), 1, b, libraryLoans);
                            libraryLoans.add(b);
                        }
                    System.out.println(e.getNodeName() + "  [CLOSE]");
                }catch (Exception e) 
                {
                     System.out.println("Exception thrown  now :" + e);
                } 
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            for(BankObject_BankLoan loan : libraryLoans)
               System.out.println( loan.getLoanData());
    
    }
    
     @Override
    public boolean quitProgram() {
        return super.quitProgram(); //To change body of generated methods, choose Tools | Templates.
    }
     @Override
    public BankObject_BankLoan deRegisterALoan() {
        
        BankObject_BankLoan loan = super.deRegisterALoan(); //To change body of generated methods, choose Tools | Templates.
        
        try{
                // skapa Documetbuilderfactory
               DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
               // skapa documetbilder
               DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
               // läsa hela XML filen 
               Document docLibraryLoans = dBuilder.parse(new FileInputStream(new File(filePathLibraryLoans)));

               //root 
               //ny elemnts
               NodeList nodeList = docLibraryLoans.getElementsByTagName("loan");

               int find=0;
               for(int i = 0; i < nodeList.getLength(); i++){
                   Node node =  nodeList.item(i);
                   Element e = (Element) node;
                   String id = e.getAttribute("id");
                   if(Integer.toString(loan.loanID).equals(id))
                   nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
                   BankSupport.WriteDOMtoXMLFile(docLibraryLoans,filePathLibraryLoans);
               }
                     
            }catch(ParserConfigurationException | SAXException | IOException | DOMException | TransformerException e){
		   System.out.println("Exception :"+e);
	   }
        
        
        return loan;
    }


    @Override
    public BankObject_BankLoan registerALoan() {
        BankObject_BankLoan loan =  super.registerALoan(); //To change body of generated methods, choose Tools | Templates.
        
        if(loan != null){
            try{
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();      
                    Document document = documentBuilder.parse(filePathLibraryLoans);
                    Element root = document.getDocumentElement();
                    Element newLoan = document.createElement("loan");
                    Attr id_Attr = document.createAttribute("id");
                    String id = Integer.toString(loan.loanID);
                    id_Attr.setValue(id);
                    newLoan.setAttributeNode(id_Attr);
                    root.appendChild(newLoan);

                     Element tagName = document.createElement("media_id");
                                    tagName.appendChild(document.createTextNode(Integer.toString(loan.bookID)));
                                    newLoan.appendChild(tagName);

                    tagName = document.createElement("user_id");
                                    tagName.appendChild(document.createTextNode(Integer.toString(loan.userID)));
                                    newLoan.appendChild(tagName);

                    tagName = document.createElement("loan_date");
                                    tagName.appendChild(document.createTextNode(loan.loanDate));
                                    newLoan.appendChild(tagName);

                    tagName = document.createElement("expires_date");
                                    tagName.appendChild(document.createTextNode(loan.expiresDate));
                                    newLoan.appendChild(tagName);

                    
                    BankSupport.WriteDOMtoXMLFile(document,filePathLibraryLoans);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
        return loan;
    }
    
   
    
    
    
    
    
    /*********************** MUST HAVE METHODS *********************************/

    @Override
    public BankObject_BankUser registerAUser() {
        return super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public BankObject_BankUser deRegisterAUser() {
        return super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean extractUserDataFromID(int id) {
        return super.extractUserDataFromID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findUserIDFromName(String fullName) {
        return super.findUserIDFromName(fullName); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchUserData() {
        super.searchUserData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean extractAccountDataFromAccountID(int id) {
        return super.extractAccountDataFromAccountID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findUserIDFromAccountID(String title) {
        return super.findUserIDFromAccountID(title); //To change body of generated methods, choose Tools | Templates.
    }

//    @Override
//    public void searchTitleData() {
//        super.searchTitleData(); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public void searchAccountData() {
        super.searchAccountData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void search() {
        super.search(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BankObject_BankAccount deRegisterAnAccount() {
        return super.deRegisterAnAccount(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BankObject_BankAccount registerAnAccount() {
        return super.registerAnAccount(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int WelcomeString() {
        return super.WelcomeString(); //To change body of generated methods, choose Tools | Templates.
    }
     @Override
    public int findLoginData(String fullName, String PassWord){
        return super.findLoginData(fullName, PassWord);
    }
    
}
