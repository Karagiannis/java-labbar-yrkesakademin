/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import dom_package.DomUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import banksupporxml.BankSupport;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Lasse
 */
public class BankEvents_BankAccountsToXMLFileHandler extends BankEventsDecorator {
    
    //LibraryEventsInterface libEvents;
    ArrayList<BankObject_BankAccount> books;
    String filePathBooks = "c:\\LOG\\books.xml";
    
    
    public BankEvents_BankAccountsToXMLFileHandler(BankEventsInterface libEvents, ArrayList<BankObject_BankAccount> books)
    {
            super(libEvents);
            this.libEvents = libEvents;
            this.books = books;
            
            
            //Transfer data from xml-file to datastructure books, every time program is started
           loadBooksFromXMLFileToDataStructure();
           
           
           
           
    }
    
    private void loadBooksFromXMLFileToDataStructure(){
        
        //Populate datastructure libraryBooks
            try{
                    
                    FileInputStream in = new FileInputStream(new File(filePathBooks));
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(in, "UTF-8");
                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    //NodeList nList = doc.getChildNodes();
                    Element e =  doc.getDocumentElement();
                    //Element first = DomUtil.getFirstElementChild(e);
                    ArrayList<Element> elemArray = DomUtil.getChildElements(e);
                    System.out.println(e.getNodeName() + "  [OPEN]");
                        for (int i = 0; i < elemArray.size(); i++)
                        {
                            System.out.println("Size "+ elemArray.size());
                           
                            BankObject_BankAccount b = new BankObject_BankAccount();
                            //if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                           
                            DomUtil.LibraryBooksFillingDataRoutine(elemArray.get(i), 1, b, books);
                            books.add(b);
                        }
                    System.out.println(e.getNodeName() + "  [CLOSE]");

                
                
                }catch (Exception e) 
                {
                     System.out.println("Exception thrown  now :" + e);
                } 
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            for(BankObject_BankAccount book : books)
               System.out.println( book.getBookData());
        
    }
     
    
   

    @Override
    public BankObject_BankAccount registerAnAccount(){
    
        BankObject_BankAccount b = super.registerAnAccount();
        if(b != null){
            try{
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();      
                    Document document = documentBuilder.parse(filePathBooks);
                    Element root = document.getDocumentElement();
                    Element newBook = document.createElement("book");
                    Attr id_Attr = document.createAttribute("id");
                    String id = Integer.toString(b.ID);
                    id_Attr.setValue(id);
                    newBook.setAttributeNode(id_Attr);
                    root.appendChild(newBook);

                     Element tagName = document.createElement("author");
                                    tagName.appendChild(document.createTextNode(b.getAuthor()));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("title");
                                    tagName.appendChild(document.createTextNode(b.getTitle()));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("genre");
                                    tagName.appendChild(document.createTextNode(b.category));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("price");
                                    tagName.appendChild(document.createTextNode(b.price));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("publish_date");
                                    tagName.appendChild(document.createTextNode(b.publish_date));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("description");
                                    tagName.appendChild(document.createTextNode(b.description));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("isavailible");
                                    tagName.appendChild(document.createTextNode("true"));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("was_loaned_lastime_at");
                                    tagName.appendChild(document.createTextNode(b.wasLoanedLastTimeAt));
                                    newBook.appendChild(tagName); 
                                    
                    tagName = document.createElement("loan_expires");
                                    tagName.appendChild(document.createTextNode(b.loanExpires));
                                    newBook.appendChild(tagName); 
                    
                    tagName = document.createElement("registered_at_date");
                                    tagName.appendChild(document.createTextNode(b.registeredAtDate));
                                    newBook.appendChild(tagName); 
                                  
                                         

                    DOMSource source = new DOMSource(document);
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer(); 
                    StreamResult result = new StreamResult(filePathBooks);
                    transformer.transform(source, result);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return b;
    }

    @Override
    public BankObject_BankAccount deRegisterAnAccount() {
        BankObject_BankAccount book =  super.deRegisterAnAccount(); //To change body of generated methods, choose Tools | Templates.
        
        BankObject_BankLoan loan = super.deRegisterALoan(); //To change body of generated methods, choose Tools | Templates.
        
        try{
                // skapa Documetbuilderfactory
               DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
               // skapa documetbilder
               DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
               // läsa hela XML filen 
               Document documnet = dBuilder.parse(new FileInputStream(new File(filePathBooks)));

               //root 
               //ny elemnts
               NodeList nodeList = documnet.getElementsByTagName("book");

               int find=0;
               for(int i = 0; i < nodeList.getLength(); i++){
                    Node node =  nodeList.item(i);
                    Element e = (Element) node;
                    String id = e.getAttribute("id");
                    if(Integer.toString(loan.loanID).equals(id))
                    nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
                    BankSupport.WriteDOMtoXMLFile(documnet,filePathBooks);
               }
                     
            }catch(ParserConfigurationException | SAXException | IOException | DOMException | TransformerException e){
		   System.out.println("Exception :"+e);
	   }
        
        
        
        return book;
    }
    
    
    
    
    @Override
    public boolean quitProgram(){
        return super.quitProgram();
    }

    @Override
    public boolean extractUserDataFromID(int id) {
        return super.extractUserDataFromID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findUserIDFromName(String fullName) {
        return super.findUserIDFromName(fullName); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchUserData() {
        super.searchUserData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean extractAccountDataFromAccountID(int id) {
        return super.extractAccountDataFromAccountID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findUserIDFromAccountID(String title) {
        return super.findUserIDFromAccountID(title); //To change body of generated methods, choose Tools | Templates.
    }

//    @Override
//    public void searchTitleData() {
//        super.searchTitleData(); //To change body of generated methods, choose Tools | Templates.
//    }

    @Override
    public void searchAccountData() {
        super.searchAccountData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void search() {
        super.search(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BankObject_BankUser registerAUser() {
        return super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public BankObject_BankUser registerAUserFromGUI(String name, String familyName, String personNumber,
            String postAdress, String postNumber, String city, String email) {
        return super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public BankObject_BankUser deRegisterAUser() {
        return super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
    }

    
    @Override
    public BankObject_BankLoan registerALoan() {
        return super.registerALoan(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public BankObject_BankLoan deRegisterALoan() {
        return super.deRegisterALoan(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int WelcomeString() {
        return super.WelcomeString(); //To change body of generated methods, choose Tools | Templates.
    }
    
    @Override
    public int findLoginData(String fullName, String PassWord){
        return super.findLoginData(fullName, PassWord);
    }
    
    
    
}
