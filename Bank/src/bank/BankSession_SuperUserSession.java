/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankSession_SuperUserSession extends BankSession_{

    ArrayList<BankCommand_> superUserCommands;
    
    public BankSession_SuperUserSession(Prompt prompt,
            ArrayList<BankObject_BankAccount> accounts,
            ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            BankEventsInterface bankEvents)
    {
        super(prompt, accounts, bankUsers, bankLoans, bankEvents);
        
        superUserCommands = new ArrayList<>();
        superUserCommands.add(new BankCommand_RegisterAccountCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        superUserCommands.add(new BankCommand_RegisterUserCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        superUserCommands.add(new BankCommand_RegisterLoanCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        superUserCommands.add(new BankCommand_DeRegisterAccountCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        superUserCommands.add(new BankCommand_SearchCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        superUserCommands.add(new BankCommand_LogoutCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
    
    }

    @Override
    public void start() 
    {
        
        if(login())
        {
        
            String input;
           int inputCode = 0;
           do{
               for (int i = 0; i < superUserCommands.size(); i++)
               {
                   prompt.println(i + " - " + superUserCommands.get(i).getCommand());

               }
               try{
                    prompt.print("Vad vill du göra? >");
                    inputCode = Integer.parseInt( prompt.readLine());
                    if(inputCode > 2 || inputCode < 0)
                        throw new Exception();
                    else
                        superUserCommands.get(inputCode).execute();
                  }catch(Exception e){
                      prompt.println("Måste ange siffrorna 0 till 2");
                      prompt.println(e.toString());
                  }
           }while(inputCode != 2);
        }else{
                   
                prompt.println("Försök igen");
           }
    }
        
     @Override
    public boolean login(){
     
    boolean temp = super.login();
     
    return true;
    }
    
}
