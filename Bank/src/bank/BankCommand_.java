/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Lasse
 */
 public abstract class BankCommand_ {
     
        protected final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ArrayList<BankObject_BankUser> libraryUsers = null;
        ArrayList<BankObject_BankLoan> libraryLoans = null;
        ArrayList<BankObject_BankAccount> libraryBooks = null;
        Prompt prompt = null;
        BankEventsInterface bankEvents;
                
     
     
     public BankCommand_(ArrayList<BankObject_BankUser> libraryUsers,
                        ArrayList<BankObject_BankLoan> libraryLoans,
                        ArrayList<BankObject_BankAccount> libraryBooks,
                        Prompt prompt,
                       BankEventsInterface libEvents){
                this.libraryUsers = libraryUsers;
                this.libraryLoans = libraryLoans;
                this.libraryBooks = libraryBooks;
                this.prompt = prompt;
                this.bankEvents = libEvents;
     }
     
     public BankCommand_(ArrayList<BankObject_BankLoan> libraryLoans,
                                ArrayList<BankObject_BankAccount> libraryBooks, Prompt prompt,
                                BankEventsInterface libEvents){
         this.prompt = prompt;
         this.libraryBooks = libraryBooks;
         this.libraryLoans = libraryLoans;
         this.bankEvents = libEvents;
     }
    
     public BankObject_ execute(){
     
         return onExecute();
     }
    
     protected abstract String getCommand();
    
     protected abstract BankObject_ onExecute();  
     
     protected String getTodaysDate() {
        return dateFormat.format(new Date());//To change body of generated methods, choose Tools | Templates.
    }

    protected String getExpirationDate() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 30); //minus number would decrement the days
        date = cal.getTime();
        return dateFormat.format(date); //To change body of generated methods, choose Tools | Templates.
    }

}
