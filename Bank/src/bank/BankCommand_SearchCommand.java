/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Lasse
 */
public class BankCommand_SearchCommand extends BankCommand_{
    
    
    public BankCommand_SearchCommand(ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            ArrayList<BankObject_BankAccount> bankAccounts,
            Prompt prompt,
            BankEventsInterface bankEvents){
            super(bankUsers, bankLoans,bankAccounts,prompt, bankEvents);
    
    }

    @Override
    protected String getCommand() {
        return "Sökning:";
    }

    @Override
    protected BankObject_ onExecute() {
        
        bankEvents.search();
        return null;
//        String searchCategories =   "\n1 - Titel (bok, cd/dvd, tidning)\n"+
//                                       "2 - Författare\n" +
//                                       "3 - Låntagar info \n" +
//                                       "4 - Avsluta sökning\n" +
//                                       "Vad vill du söka? >";
//        int categoryId;
//      do{
//            String categoryIdString = prompt.ask(searchCategories);
//            categoryId = Integer.parseInt(categoryIdString);
//            switch (categoryId)
//            {
//                case 1:
//                    searchTitleData();
//                    break;
//                case 2:
//                    searchAccountData();
//                    break;
//                case 3:
//                    searchUserData();
//                    break;
//                case 4:
//                    break; 
//            }       
//        }while(categoryId != 4);
//      return null;
//    }
//    
//    /********Utility method with pattern matchin not currently used**********/
//    private boolean matchString(String input, String expression){
//         String REGEX = expression;
//         String INPUT = input;
//                                    
//   
//       Pattern p = Pattern.compile(REGEX);
//       Matcher m = p.matcher(INPUT); // get a matcher object
//     
//       if(m.find()) {
//         System.out.println("Found match");
//         return true;
//       }
//       else return false;
//    }
//    
//    
//    /*************Searching      Author data related************************/
//    public void searchAccountData(){
//        String author = prompt.ask("Skriv författarens namn >");
//        boolean found = false;
//        for(BankObject_BankAccount book : libraryBooks){
//           if (book.getAuthor().equals(author)) {
//               found = true;
//               System.out.println(book.getBookData());
//            }
//        }
//        if(!found)
//            System.out.println("Author was not found");
//    }
//    
//    /****************Searching        LibraryBook related***************************/
//    
//    /**
//     * Searching        BankObject_BankAccount related
//     */
//    public void searchTitleData(){
//        String title = prompt.ask("Skriv titeln >");
//        extractAccountDataFromAccountID(findBookIDFromTitle(title));
//    }
//    
//    
//    public int findBookIDFromTitle(String title){
//        int bookID = -1;
//        boolean found = false;
//        for(BankObject_BankAccount book : libraryBooks){
//           if (book.getTitle().equals(title)) {
//               bookID = book.getBookID();
//               found = true;
//            }
//        }
//        if(!found)
//            System.out.println("User was not found");
//        
//        return bookID; 
//    }
//    
//    
//    public boolean extractAccountDataFromAccountID(int id){
//        boolean found = false;
//        if(id != -1)
//        {
//            for(BankObject_BankAccount book : libraryBooks){
//               if (book.getBookID() == id) {
//                   found = true;
//                   System.out.println("Bokens registrerade uppgifter:\n" + book.getBookData());
//                }
//            }
//        }
//        if(!found)
//            System.out.println("Boken finns inte");
//        
//        return found;
//    }
//         
//    
//  /**************Searching       User data related**************************/  
//    
//    
//    public void searchUserData(){
//        String name = prompt.ask("Skriv låntagarens namn >");
//        extractUserDataFromID(findUserIDFromName(name)); 
//    }
//    
//     
//    public int findUserIDFromName(String fullName){
//        int userID = -1;
//        boolean found = false;
//        for(BankObject_BankUser user : libraryUsers){
//           if (user.getName().equals(fullName)) {
//               userID = user.getID();
//               found = true;
//               extractUserDataFromID(userID);
//            }
//        }
//        if(!found)
//            System.out.println("User was not found");
//        
//        return userID; 
//    }
//    
//    
//  
//    public boolean extractUserDataFromID(int id){
//        boolean found = false;
//        for(BankObject_BankUser user : libraryUsers){
//           if (user.getID() == id) {
//               found = true;
//               
//               System.out.println("Användarens registrerade uppgifter:\n" + user.getUserData());
//               
//               
//               ArrayList<LibraryObject_LibraryLoan> loans = user.getUserLoans();
//               if(loans.size() > 0 )
//                   System.out.println("Användaren har tidigare registrerade lån\n");
//               for (BankObject_BankLoan loan : loans)
//                    for(BankObject_BankAccount book : libraryBooks)
//                        if(book.getBookID() == loan.bookID)
//                            System.out.println(book.getBookData());
//            }
//        }
//        if(!found)
//            System.out.println("User was not found");
//        
//        return found;
    }
    
}
