/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Lasse
 */
public class BankObject_BankAccount extends BankObject_{
    public int accountID = 0;
    public String accountCategory = "";
    public String nameOfAccountHolder = "";
    public double accountSum = 0.0;
    public String description;    //A description of why the bank agreed on this service to this particular customer
    public boolean isAvailible = true;//Account is not frozen
    public double interestRate = 0.0;
    public String accountExpires = "1970-01-01";
    public String registeredAtDate;
    public boolean hasLoanAttachedToAccount = false;
    public int loanID = -1;
    public BankObject_BankLoan loan;
    
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public BankObject_BankAccount(int ID, String nameOfAccountHolder, String author, String accountCategory ){
        this.accountID = ID;
        
        this.nameOfAccountHolder = nameOfAccountHolder;
        this.accountCategory = accountCategory;
        registeredAtDate = getDateOfToday();
        
    }
    
    public BankObject_BankAccount(){}
    
    public int getAccountID(){
        return this.accountID;
    }
    
    public BankObject_BankAccount getAccount(int accountID){
        
        if(accountID == this.accountID)
            return this;
        else
            return null;
        
    }
    
    public String getNameOfAccountHolder(){
        return this.nameOfAccountHolder;
    }
     
   
    
    public String getAccountData(){
        String data1 = " ID: "+ Integer.toString(accountID) +"\n";
        String data2 = "accountCategory: " + accountCategory +"\n";
        String data3 = "nameOfAccountHolder: " + nameOfAccountHolder +"\n";
        String data4 = "accountSum: " + Double.toString(accountSum)+"\n";
        String data5 = "description: "+ description +"\n";
        String data6 = "isAvailible: " +Boolean.toString(isAvailible) +"\n";
        String data7 = "interestRate: " + Double.toString(interestRate) +"\n";
        String data8 = "accountExpires: "+ accountExpires +"\n";
        String data9 = "registeredAtDate: " + registeredAtDate +"\n";
        
   
        
       return data1 + data2 + data3 + data4 + data5 + data6 + data7 + data8 + data9;
    }
    
    public boolean createLoan(int loanID,double amount, double interestRate, double amortizationPerMonth){
       
        if(isAvailible){
         this.loanID = loanID;
         loan = new BankObject_BankLoan(loanID,accountID,amount,interestRate, amortizationPerMonth); 
         return true;
        }else
            return false;
        
        
    }
     private String getDateOfToday() {
        return dateFormat.format(new Date());
    }
     
    public boolean isAvailibe(){
        return isAvailible;
    }
    
    public void setAsUnAvailible(){
    
        this.isAvailible = false;
    }
    
    public double insertMoneyToAccount(double money){
        accountSum += money;
        return accountSum;    
    }
   
    public double amortizationOnLoan(double repayment){
        return 0.0;
    }
    public void transferLoanedAmountToAccount(){
        accountSum = loan.transferAmountThatisLoanedToAccount();
    }
    
}
