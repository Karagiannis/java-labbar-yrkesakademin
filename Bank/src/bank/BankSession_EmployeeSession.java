/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankSession_EmployeeSession extends BankSession_{

    ArrayList<BankCommand_> employeeCommands;
    
        
   
    public BankSession_EmployeeSession(Prompt prompt,
            ArrayList<BankObject_BankAccount> bankAccounts,
            ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            BankEventsInterface bankEvents)
            
     {
        super(prompt,bankAccounts,bankUsers, bankLoans, bankEvents);
        employeeCommands = new ArrayList<>();
        employeeCommands.add(new BankCommand_RegisterAccountCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        employeeCommands.add(new BankCommand_RegisterUserCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        employeeCommands.add(new BankCommand_RegisterLoanCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        employeeCommands.add(new BankCommand_DeRegisterAccountCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        employeeCommands.add(new BankCommand_SearchCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        employeeCommands.add(new BankCommand_LogoutCommand(bankUsers,bankLoans,bankAccounts,prompt,bankEvents));
        

    }

    

    /**
     *
     */
    @Override
    public void start() 
    {
        BankObject_ obj;
        String input;
        int inputCode = 0;
        do{
            for (int i = 0; i < employeeCommands.size(); i++){
                prompt.println(i + " - " + employeeCommands.get(i).getCommand());

            }
            try{
                prompt.print("Vad vill du göra? >");
                 inputCode = Integer.parseInt( prompt.readLine());
                 if(inputCode > 5 || inputCode < 0)
                     throw new Exception();
                 else
                    obj =  employeeCommands.get(inputCode).execute();
               }catch(Exception e){
                   prompt.println("Måste ange siffrorna 1 till 6");
                   prompt.println(e.toString());
               }
            
            
        }while(inputCode != 5);
    }
    
    @Override
    public boolean login(){
     
    boolean temp = super.login();
    
    return true;
     
    }
    
    
}
