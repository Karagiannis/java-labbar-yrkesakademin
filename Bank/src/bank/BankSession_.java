/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public abstract class BankSession_ {


    Prompt prompt;
    ArrayList<BankObject_BankAccount> bankAccounts;
    ArrayList<BankObject_BankUser> bankUsers;
    ArrayList<BankObject_BankLoan> bankLoans;
    private String userName;
    private String passWord;
    BankObject_ object;
    BankEventsInterface bankEvents;


public BankSession_(Prompt prompt,
            ArrayList<BankObject_BankAccount> bankAccounts,
            ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            BankEventsInterface bankEvents)
     {
        this.prompt = prompt;
        this.bankAccounts = bankAccounts;
        this.bankUsers = bankUsers;
        this.bankLoans = bankLoans;
        this.bankEvents =  bankEvents;
}

    
    public abstract void start();
    
    public boolean login(){
        
        userName = prompt.ask("Ange användar namn >");   
        //passWord = prompt.readPassword("Ange lösenord >");
        passWord = prompt.ask("Ange lösenord >");
     
        return true;
    }
}


