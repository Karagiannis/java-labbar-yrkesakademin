/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

/**
 *
 * @author Lasse
 */
public class BankObject_BankLoan extends BankObject_{
    

    public int loanID;
    public int accountID;
    public String loanDate;
    public String expiresDate;
    public double amountThatIsLoaned;
    public double amortizationPerMonth;
    public double interestRate;
    public double currentSumToBeRepayed;
    private boolean transferredLoanedAmountToAccount = false;
   
  
    
    public BankObject_BankLoan(int loanID, int accountID, double amount, double interestRate, double amortizationPerMonth){
        this.loanID = loanID;
        this.accountID = accountID;
        this.amountThatIsLoaned = amount;
        currentSumToBeRepayed = this.amountThatIsLoaned;
        this.interestRate = interestRate;
    }
    
    public BankObject_BankLoan(){
    }
    
    public BankObject_BankLoan(BankObject_BankAccount b){
        this.accountID = b.getAccountID();
    }
    
   public BankObject_BankLoan getLoanID(int loanID){
        if (loanID == this.loanID)
            return this;
        else
            return null;
    }
   
   public BankObject_BankLoan getAccountID(int accountID){
        if (accountID == this.accountID)
            return this;
        else
            return null;
    }
   
   public int getAccountIDFromLoanObject(){
       return this.accountID;
   }
  public String getLoanData(){
  
      String s = String.format("Loan ID: %d\nAccount ID: %d\n Amount that was loaned: %d\nLoan Date: %s\nExpires Date : %s\nInterest rate: %d\n Current amount to repay %d",
              loanID,accountID, amountThatIsLoaned,loanDate,expiresDate,interestRate,currentSumToBeRepayed );
      return s;
   
  }
  public void doRepayment(double amount){
      currentSumToBeRepayed -= amount;
  }
  
  public void updateSumWithInterestRate(){
      currentSumToBeRepayed *= interestRate;
  }
  public double transferAmountThatisLoanedToAccount(){
      
      if(!transferredLoanedAmountToAccount){
          transferredLoanedAmountToAccount = true;
          return amountThatIsLoaned;
      }else return 0.0;
  }
}
