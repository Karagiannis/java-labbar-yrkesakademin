/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankCommand_DeRegisterAccountCommand extends BankCommand_{
    
    BankCommand_DeRegisterAccountCommand(ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            ArrayList<BankObject_BankAccount> bankAccounts,
            Prompt prompt,
            BankEventsInterface bankEvents){
            super(bankUsers, bankLoans,bankAccounts,prompt, bankEvents);
    }

    @Override
    protected String getCommand() {
        return "Avregistrera ett lån"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected BankObject_ onExecute() {
        
        return bankEvents.deRegisterALoan();
//        BankObject_BankLoan loan = null;
//        String userID = prompt.ask("Låntagarens ID: ");
//        int iUserID = Integer.parseInt(userID);
//        
//        String bookID = prompt.ask("Bokens ID: ");
//        int ibookID  = Integer.parseInt(userID);
//        
//        for(BankObject_BankUser libraryUser : libraryUsers){
//            if(iUserID == libraryUser.userID){
//                libraryUser.unregisterALoan(ibookID);
//            }       
//        }
//        return loan;
    }
    
}
