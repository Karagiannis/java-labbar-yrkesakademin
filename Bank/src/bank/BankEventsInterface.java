/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

/**
 *
 * @author Lasse
 */
public interface BankEventsInterface {

    int WelcomeString();

    BankObject_BankAccount registerAnAccount();
    
    
    BankObject_BankAccount deRegisterAnAccount();

    BankObject_BankLoan registerALoan();
    BankObject_BankLoan deRegisterALoan();
    
    BankObject_BankUser registerAUser();
    
    BankObject_BankUser registerAUserFromGUI(String name, String familyName, String personNumber,
            String postAdress, String postNumber, String city, String email);
    
    BankObject_BankUser deRegisterAUser();
    
    void search();
    
    void searchAccountData();
    
    int findUserIDFromAccountID(String title);

    boolean extractAccountDataFromAccountID(int id);
    
    void searchUserData();

    int findUserIDFromName(String fullName);

    boolean extractUserDataFromID(int id);
    
    boolean quitProgram();
    int findLoginData(String fullName, String PassWord);

    
    
}
