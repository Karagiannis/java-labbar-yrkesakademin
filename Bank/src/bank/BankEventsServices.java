/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankEventsServices {
    
    ArrayList<BankObject_BankAccount> bankAccounts;
    ArrayList<BankObject_BankUser> bankUsers;
    ArrayList<BankObject_BankLoan> bankLoans;
    
    public BankEventsServices(ArrayList<BankObject_BankAccount> bankAccounts, ArrayList<BankObject_BankUser> bankUsers,
                                     ArrayList<BankObject_BankLoan> bankLoans){
        this.bankAccounts = bankAccounts;
        this.bankUsers = bankUsers;
        this.bankLoans = bankLoans;
    
    }
    
    public int findUserCategoryFromLoginData(String loginName, String passWord){
        System.out.println("Inside findUserCategoryFromLoginData");
        int userCategoryID = -1;//Not found
        boolean found = false;
        for(BankObject_BankUser user : bankUsers)
        {
           if (user.getName().equals(loginName)) 
           {
               System.out.println("User was found " + user.getName());
               if(user.getUserPassWord().equals(passWord))
               {
                   System.out.println("Password was found: " +user.getUserPassWord());
                   found = true;
                   if(user.category.equals("customer"))
                   {
                       System.out.println("You are a custimer");
                       return 1;
                   }
                   else if(user.category.equals("admin"))
                   {
                       System.out.println("Hallo admin");
                       System.out.println("You are" + user.getName());
                       return 2;
                   }
                   else if(user.category.equals("superuser"))
                   {
                       System.out.println("Hallo super user");
                       System.out.println("You are" + user.getName());
                       return 3;
                   }
                }
            }
        }
        
        if(!found)
            System.out.println("User was not found");
        return userCategoryID; 
       
    }
    
}
