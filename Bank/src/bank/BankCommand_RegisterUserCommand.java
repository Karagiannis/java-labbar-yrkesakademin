/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankCommand_RegisterUserCommand extends BankCommand_{
    
    
    public BankCommand_RegisterUserCommand(ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            ArrayList<BankObject_BankAccount> bankAccounts,
            Prompt prompt,
            BankEventsInterface bankEvents){
        
            super(bankUsers, bankLoans,bankAccounts,prompt, bankEvents);
    
    
    }

    @Override
    protected String getCommand() {
        return "Registrera en användare";
    }

    @Override
    protected BankObject_ onExecute() {
        
        return bankEvents.registerAUser();
//        String name = prompt.ask("Ny låntagares, förnamn: >");
//        String familyName = prompt.ask("Ny låntagares, efternamn: >");
//        String personNumber = prompt.ask("Ny låntagares, personnummer (yy-mm-dd-xxxx) >");
//        String postAdress = prompt.ask("Ny låntagares, post-adress: >");
//        String postNumber = prompt.ask("Ny låntagares, postnummer : >");
//        String city = prompt.ask("Ny låntagares, ort: >");
//        String email = prompt.ask("Ny låntagares, e-mail adress: >");
//        
//        int numberIDOfNewUser = libraryUsers.size();
//        BankObject_BankUser user = new BankObject_BankUser(numberIDOfNewUser, name, familyName);
//        user.personNumber = personNumber;
//        user.postAdress = postAdress;
//        user.postNumber = postNumber;
//        user.city = city;
//        user.email = email;
//        
//        libraryUsers.add(user);
//        
//        System.out.println("Du registrerade användar ID: " + numberIDOfNewUser );
//        System.out.println("förnamn: " + name);
//        System.out.println("efternamn: " + familyName);
//        return user;//To change body of generated methods, choose Tools | Templates.

           
    }
    
}
