/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public abstract class BankEventsDecorator implements BankEventsInterface {
    
    BankEventsInterface libEvents;
    
    
    public ArrayList<BankObject_BankAccount> books;
    public ArrayList<BankObject_BankUser> libraryUsers;
    protected final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected  Prompt prompt;

    public BankEventsDecorator( BankEventsInterface libEvents) {
        this.libEvents = libEvents;
    }

    /*
        int WelcomeString(); //OK

    BankObject_BankAccount registerAnAccount();//OK
    
    
    BankObject_BankAccount deRegisterAnAccount();//OK

    BankObject_BankLoan registerALoan();//OK
    BankObject_BankLoan deRegisterALoan();//OK
    
    BankObject_BankUser registerAUser();//OK
    
    BankObject_BankUser registerAUserFromGUI(String name, String familyName, String personNumber,
            String postAdress, String postNumber, String city, String email);//OK
    
    BankObject_BankUser deRegisterAUser();  //OK
    
    void search();  //OK
    
    void searchAccountData();
    
    int findUserIDFromAccountID(String title);//OK

    boolean extractAccountDataFromAccountID(int id);//OK
    
    void searchUserData();  //OK

    int findUserIDFromName(String fullName);  //OK

    boolean extractUserDataFromID(int id);  //OK
    
    boolean quitProgram();
    int findLoginData(String fullName, String PassWord);
    
    */
    
    
    @Override
    public int WelcomeString(){//OK
        return libEvents.WelcomeString();
    }
    /****************************************************************/
    /************************* Registration event
     * @return s******************/
    @Override
    public  BankObject_BankLoan deRegisterALoan(){//OK
        return libEvents.deRegisterALoan();
    }
    /******************** Registation        bookloan *************************/
    @Override
    public BankObject_BankLoan registerALoan(){//OK
        return libEvents.registerALoan();
    }
    /*******************Registration       book data**************************/
    @Override
    public BankObject_BankAccount registerAnAccount(){//OK
    
        return libEvents.registerAnAccount();
    }

    @Override
    public  BankObject_BankAccount deRegisterAnAccount(){//OK
        return libEvents.deRegisterAnAccount();
    }
    
    

    /****************************  Registration  user data******************************/
    @Override
    public BankObject_BankUser registerAUser(){  //OK
        return libEvents.registerAUser();
    }

    @Override
    public BankObject_BankUser registerAUserFromGUI(String name, String familyName, String personNumber,
            String postAdress, String postNumber, String city, String email){  //OK
        return libEvents.registerAUser();
    }
    @Override
    public abstract BankObject_BankUser deRegisterAUser();  //OK

   

    /**********************************************************************/
    /***********************Searching**************************************/
   @Override
    public  void search(){  //OK
        libEvents.search();
    }

    /********Utility method with pattern matchin not currently used**********/
    
    

    /*************Searching      Author data related************************/
    @Override
    public  void searchAccountData(){  //OK
        libEvents.searchAccountData();
    }

    /****************Searching        BankObject_BankAccount related*************************/
//    @Override
//    public void searchTitleData(){
//        libEvents.searchTitleData();
//    }

    @Override
    public  int findUserIDFromAccountID(String title){
    
        return libEvents.findUserIDFromAccountID(title);  //OK
    }

    @Override
    public boolean extractAccountDataFromAccountID(int id){  //OK
    
        return libEvents.extractAccountDataFromAccountID(id);
    }

    /**************Searching       User data related**************************/
    @Override
    public void searchUserData(){   //OK
        libEvents.searchUserData();
    
    }

    @Override
    public  int findUserIDFromName(String fullName){  //OK
        return libEvents.findUserIDFromName(fullName);
    }

    @Override
    public  boolean extractUserDataFromID(int id){  //OK
        return libEvents.extractUserDataFromID(id);
    }
    @Override
    public  boolean quitProgram(){
        return libEvents.quitProgram();
    }
    @Override
    public int findLoginData(String fullName, String PassWord){
        return libEvents.findLoginData(fullName, PassWord);
    }
    
}
