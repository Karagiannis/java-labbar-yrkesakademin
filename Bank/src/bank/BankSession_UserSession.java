/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankSession_UserSession extends BankSession_{

    ArrayList<BankCommand_> customerCommands;
    
    public BankSession_UserSession(Prompt prompt,
            ArrayList<BankObject_BankAccount> accounts,
            ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            BankEventsInterface bankEvents)
    {
        super(prompt, accounts, bankUsers, bankLoans, bankEvents);
        
        customerCommands = new ArrayList<>();
        customerCommands.add(new BankCommand_RegisterLoanCommand(bankUsers,bankLoans,accounts,prompt,bankEvents));
        customerCommands.add(new BankCommand_SearchCommand(bankUsers,bankLoans,accounts,prompt,bankEvents));
        customerCommands.add(new BankCommand_LogoutCommand(bankUsers,bankLoans,accounts,prompt,bankEvents));
       
        
    
    }

    @Override
    public void start() 
    {
        
        if(login())
        {
        
            String input;
           int inputCode = 0;
           do{
               for (int i = 0; i < customerCommands.size(); i++)
               {
                   prompt.println(i + " - " + customerCommands.get(i).getCommand());

               }
               try{
                    prompt.print("Vad vill du göra? >");
                    inputCode = Integer.parseInt( prompt.readLine());
                    if(inputCode > 2 || inputCode < 0)
                        throw new Exception();
                    else
                        customerCommands.get(inputCode).execute();
                  }catch(Exception e){
                      prompt.println("Måste ange siffrorna 0 till 2");
                      prompt.println(e.toString());
                  }
           }while(inputCode != 2);
        }else{
                   
                prompt.println("Försök igen");
           }
    }
        
     @Override
    public boolean login(){
     
    boolean temp = super.login();
     
    return true;
    }
    
}
