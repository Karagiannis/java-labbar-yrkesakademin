/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class BankCommand_LogoutCommand extends BankCommand_{
    
    public BankCommand_LogoutCommand(ArrayList<BankObject_BankUser> banksUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            ArrayList<BankObject_BankAccount> bankAccounts,
            Prompt prompt,
            BankEventsInterface bankEvents){
            super(banksUsers, bankLoans,bankAccounts,prompt, bankEvents);
    
    }

    @Override
    protected String getCommand() {
        return "Logga ut";
    }

    @Override
    protected BankObject_ onExecute() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }
    
}
