/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Lasse
 */
public class BankCommand_RegisterLoanCommand extends BankCommand_{
        
        
    
        public BankCommand_RegisterLoanCommand(ArrayList<BankObject_BankUser> bankUsers,
            ArrayList<BankObject_BankLoan> bankLoans,
            ArrayList<BankObject_BankAccount> bankAccounts,
            Prompt prompt,
            BankEventsInterface bankEvents){
            super(bankUsers, bankLoans,bankAccounts,prompt, bankEvents);
        
            
        }
        
        @Override
        public  String getCommand(){
            return "Registrera lån";
        }    
    
        @Override
        protected  BankObject_BankLoan onExecute(){
            
            return bankEvents.registerALoan();
        
//        BankObject_BankLoan loan = null;
//        String userID = prompt.ask("Låntagarens ID: ");
//        int iUserID = Integer.parseInt(userID);
//        System.out.println(String.format("%d",iUserID ));
//        
//        String bookID = prompt.ask("Bokens ID: ");
//        int ibookID  = Integer.parseInt(bookID);
//        System.out.println(String.format("Boken som söks har ID %d",ibookID ));
//        
//        for(BankObject_BankUser libraryUser : libraryUsers){
//            if(iUserID == libraryUser.userID)
//            {
//                System.out.println("Användaren finns i databasen");
//                for(BankObject_BankAccount book : libraryBooks){
//                    System.out.println(String.format("Bokens som söks är %d, hittar %d",ibookID, book.getBookID() ));
//                    if (book.getBookID() == ibookID)
//                    {   System.out.println("Boken existerar");
//                        if(book.isAvailibe()){
//                            System.out.println("Boken finns tillgänglig");
//                            //libraryUser.registerALoan(book);
//                            //String loanExpires = book.loan();
//                            
//                            book.setAsUnAvailible();
//                            System.out.println("Lånet har registrerats");
//                            //System.out.println(loanExpires);
//                            loan = new BankObject_BankLoan();
//                            loan.bookID = book.getBookID();
//                            loan.loanID = (int) (libraryLoans.size() + Math.floor( Math.random() * 11111111.1 ));
//                            loan.loanDate = getTodaysDate();
//                            book.wasLoanedLastTimeAt = loan.loanDate;
//                            loan.expiresDate = getExpirationDate();
//                            book.loanExpires = loan.expiresDate;
//                            loan.userID = iUserID;
//                            libraryLoans.add(loan);
//                            return loan;
//                        }
//                        else{
//                            System.out.println("Boken är inte tillgänglig");
//                            break;
//                        }
//                    }else{System.out.println("Söker boken/mediat");}
//                }
//             break;
//            } else System.out.println("Söker användaren...");
//        }//end for
//        return loan;
            
        }   
       
        
}
