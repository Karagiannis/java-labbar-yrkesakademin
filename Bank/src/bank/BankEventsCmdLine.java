/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bank;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Lasse
 */
public class BankEventsCmdLine implements BankEventsInterface {
    
        ArrayList<BankObject_BankAccount> bankAccounts;
        ArrayList<BankObject_BankUser> bankUsers;
        ArrayList<BankObject_BankLoan> bankLoans;
        Prompt prompt;
        private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        BankEventsServices services;
    
    
    public BankEventsCmdLine(ArrayList<BankObject_BankAccount> bankAccounts, ArrayList<BankObject_BankUser> bankUsers,
                                     ArrayList<BankObject_BankLoan> bankLoans, Prompt prompt){
        
        
        this.bankAccounts = bankAccounts;
        this.bankUsers = bankUsers;
        this.bankLoans = bankLoans;
        this.prompt = prompt;
        services = new BankEventsServices(bankAccounts,bankUsers,bankLoans);
        
    }
    
   
    
    @Override
    public int WelcomeString()  //OK
    {
      
      
      String questionLoginUserName = " - Användar nam  >";
      String questionLoginPassword = " - Lösenord  >";
      
      
      int loginCode = 0;
      int categoryID = 0;
      String username ="";
      String password = "";
      username = prompt.ask(questionLoginUserName);
      password = prompt.ask(questionLoginPassword);
      return services.findUserCategoryFromLoginData(username, password);
      
    }
      
   
   
    
    @Override
    public BankObject_BankLoan deRegisterALoan(){ //OK
        
        BankObject_BankLoan loan = null;
        String userID = prompt.ask("Låntagarens ID: ");
        int iUserID = Integer.parseInt(userID);
        
        String bookID = prompt.ask("Bokens ID: ");
        int ibookID  = Integer.parseInt(userID);
        
        for(BankObject_BankUser libraryUser : bankUsers){
            if(iUserID == libraryUser.userID){
                libraryUser.unregisterALoan(ibookID);
            }       
        }
        return loan;
    }
    /******************** Registation        bookloan *************************/
    @Override
    public BankObject_BankLoan registerALoan(){ //OK
        BankObject_BankLoan loan = null;
        String userID = prompt.ask("Låntagarens ID: ");
        int iUserID = Integer.parseInt(userID);
        System.out.println(String.format("%d",iUserID ));
        
        String bookID = prompt.ask("Bokens ID: ");
        int ibookID  = Integer.parseInt(bookID);
        System.out.println(String.format("Boken som söks har ID %d",ibookID ));
        
        for(BankObject_BankUser libraryUser : bankUsers){
            if(iUserID == libraryUser.userID)
            {
                System.out.println("Användaren finns i databasen");
                for(BankObject_BankAccount book : bankAccounts){
                    System.out.println(String.format("Bokens som söks är %d, hittar %d",ibookID, book.getBookID() ));
                    if (book.getBookID() == ibookID)
                    {   System.out.println("Boken existerar");
                        if(book.isAvailibe()){
                            System.out.println("Boken finns tillgänglig");
                            //libraryUser.registerALoan(account);
                            //String loanExpires = account.loan();
                            
                            book.setAsUnAvailible();
                            System.out.println("Lånet har registrerats");
                            //System.out.println(loanExpires);
                            loan = new BankObject_BankLoan();
                            loan.bookID = book.getBookID();
                            loan.loanID = (int) (bankLoans.size() + Math.floor( Math.random() * 11111111.1 ));
                            loan.loanDate = getTodaysDate();
                            book.wasLoanedLastTimeAt = loan.loanDate;
                            loan.expiresDate = getExpirationDate();
                            book.loanExpires = loan.expiresDate;
                            loan.userID = iUserID;
                            bankLoans.add(loan);
                            return loan;
                        }
                        else{
                            System.out.println("Boken är inte tillgänglig");
                            break;
                        }
                    }else{System.out.println("Söker boken/mediat");}
                }
             break;
            } else System.out.println("Söker användaren...");
        }//end for
        return loan;
    }
    
    
    
    /*******************Registration       account data*************************/
    @Override
    public BankObject_BankAccount registerAnAccount(){ //OK
        
    BankObject_BankAccount account = null;   
        
     String title = prompt.ask("Bokens titel: ");
     String author = prompt.ask("Bokens författare (Efternamn, Namn): ");
     String availibleCategories =   "Välj kategori\n" +
                                    "1 - Teknik\n"+
                                    "2 - Skönlitteratur\n" +
                                    "3 - Tdining \n" +
                                    "4 - CD/DVD\n" ;
     
     
     String categoryIdString = prompt.ask(availibleCategories);
     String category = "";
     int categoryId;
     try{
        
        categoryId = Integer.parseInt(categoryIdString);
        switch (categoryId){

            case 1:
                category = "Teknik";
                break;
            case 2:
                category = "Skönlitteratur";
                break;
            case 3:
                category = "Tidning";
                break;
            case 4:
                category = "CD/DVD";
                break;

        }
     }catch(Exception e){
              System.out.println("Måste ange siffrorna 1 till 4");
              System.out.println(e);
              return account;    
          }
     
     String price = prompt.ask("Pris");
     String publishDate = prompt.ask("Publiceringsdatum ÅÅÅÅ-MM-DD: ");
     String description = prompt.ask("Beskrivning: ");
     Boolean isAvailible = true;
     String wasLoanedLastTime = " 1970-01-01";
     String loanExpires = " 1970-01-01";
     String today =  getTodaysDate();
     account = new BankObject_BankAccount( bankAccounts.size(), title, author, category );
     account.price = price;
     account.publish_date = publishDate;
     account.description = description;
     account.isAvailible = true;
     account.wasLoanedLastTimeAt = wasLoanedLastTime;
     account.loanExpires = loanExpires;
     account.registeredAtDate = today;
     
     
     
     bankAccounts.add(account);
     System.out.println("You registered book ID: " + bankAccounts.size() );
     System.out.println("title: " + title);
     System.out.println("author: " + author);
     System.out.println("category: " + category );
     
      return account;    
    }
    
    public BankObject_BankAccount deRegisterAnAccount(){ //OK
    
        BankObject_BankAccount b = null;
        String bookIDString = prompt.ask("Ange bok ID som skall avregistreras >");
        int bookID = Integer.parseInt(bookIDString);
        for (BankObject_BankAccount book : bankAccounts){
           if (book.getBook(bookID) != null){
               b = book.getBook(bookID);
               break;
           }
        }
        if(b != null)
        {
            System.out.println(b.getBookData());
            String yesNoAnswer = prompt.ask("Är det denna som ska  avregistreras? (y/n) >");
            if(yesNoAnswer.equals("y") ||yesNoAnswer.equals("Y") ){
               bankAccounts.remove(b); 
               System.out.println("Boken avregistrerades");
               return b;
            }
            else b = null;
        }else{
            System.out.println("Boken finns ej registrerad");
        }
        return b;
    
    }
     /****************************  Registration  user dat
     * @return a******************************/
    @Override
    public BankObject_BankUser registerAUser(){  //OK
        String category = prompt.ask("Ny låntagare kategori (customer/admin) >");
        String name = prompt.ask("Ny låntagares, förnamn: >");
        String familyName = prompt.ask("Ny låntagares, efternamn: >");
        String personNumber = prompt.ask("Ny låntagares, personnummer (yy-mm-dd-xxxx) >");
        String postAdress = prompt.ask("Ny låntagares, post-adress: >");
        String postNumber = prompt.ask("Ny låntagares, postnummer : >");
        String city = prompt.ask("Ny låntagares, ort: >");
        String email = prompt.ask("Ny låntagares, e-mail adress: >");
        String passWord = prompt.ask("Ny låntagares, e-mail adress: >");
        
        int numberIDOfNewUser = bankUsers.size();
        BankObject_BankUser user = new BankObject_BankUser(numberIDOfNewUser, name, familyName);
        user.personNumber = personNumber;
        user.postAdress = postAdress;
        user.postNumber = postNumber;
        user.city = city;
        user.email = email;
        user.passWord = passWord;
        user.category = category;
        
        bankUsers.add(user);
        
        System.out.println("Du registrerade användar ID: " + numberIDOfNewUser );
        System.out.println("förnamn: " + name);
        System.out.println("efternamn: " + familyName);
        return user;
        
    }
    @Override
    public BankObject_BankUser registerAUserFromGUI(String name, String familyName, String personNumber, //OK
            String postAdress, String postNumber, String city, String email){
        
//        String name = prompt.ask("Ny låntagares, förnamn: >");
//        String familyName = prompt.ask("Ny låntagares, efternamn: >");
//        String personNumber = prompt.ask("Ny låntagares, personnummer (yy-mm-dd-xxxx) >");
//        String postAdress = prompt.ask("Ny låntagares, post-adress: >");
//        String postNumber = prompt.ask("Ny låntagares, postnummer : >");
//        String city = prompt.ask("Ny låntagares, ort: >");
//        String email = prompt.ask("Ny låntagares, e-mail adress: >");
        
        int numberIDOfNewUser = bankUsers.size();
        BankObject_BankUser user = new BankObject_BankUser(numberIDOfNewUser, name, familyName);
        user.personNumber = personNumber;
        user.postAdress = postAdress;
        user.postNumber = postNumber;
        user.city = city;
        user.email = email;
        
        bankUsers.add(user);
        
        System.out.println("Du registrerade användar ID: " + numberIDOfNewUser );
        System.out.println("förnamn: " + name);
        System.out.println("efternamn: " + familyName);
        return user;
        
    }
    
    public BankObject_BankUser deRegisterAUser(){  //OK
       BankObject_BankUser user = null;
       
        String userIDString = prompt.ask("Ange änvändaren som skall avregistreras >");
        int userID = Integer.parseInt(userIDString);
        for (BankObject_BankUser u : bankUsers){
           if (u.getUser(userID) != null){
               user = u.getUser(userID);
               break;
           }
        }
        if(user != null)
        {
            System.out.println(user.getUserData());
            String yesNoAnswer = prompt.ask("Är det denna som ska  avregistreras? (y/n) >");
            if(yesNoAnswer.equals("y") ||yesNoAnswer.equals("Y") ){
               bankUsers.remove(user); 
               System.out.println("Användaren avregistrerades");
               return user;
            }
            else user = null;
        }else{
            System.out.println("Användaren finns ej registrerad");
        }   
       return user;
    
    }
     private String getTodaysDate() {
        return dateFormat.format(new Date());
    }
    private String getExpirationDate() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 30); //minus number would decrement the days
        date = cal.getTime();
        return dateFormat.format(date);
    }
    
     /**********************************************************************/
     /***********************Searching**************************************/
    @Override
    public void search()  //OK
    {
        String searchCategories =   "1 - Titel (bok, cd/dvd, tidning)\n"+
                                    "2 - Författare\n" +
                                    "3 - Låntagar info \n" +
                                    "4 - Avsluta sökning\n" +
                                    "Vad vill du söka? >";
        int categoryId;
      do{
            String categoryIdString = prompt.ask(searchCategories);
            categoryId = Integer.parseInt(categoryIdString);
            switch (categoryId)
            {
                case 1:
                    //searchTitleData();
                    break;
                case 2:
                    searchAccountData();
                    break;
                case 3:
                    searchUserData();
                    break;
                case 4:
                    break; 
            }       
        }while(categoryId != 4);
    }
    
    /********Utility method with pattern matchin not currently used**********/
    private boolean matchString(String input, String expression){
         String REGEX = expression;
         String INPUT = input;
                                    
   
       Pattern p = Pattern.compile(REGEX);
       Matcher m = p.matcher(INPUT); // get a matcher object
     
       if(m.find()) {
         System.out.println("Found match");
         return true;
       }
       else return false;
    }
    
    
    /*************Searching      Author data related************************/
    public void searchAccountData(){//
        String author = prompt.ask("Skriv författarens namn >");
        boolean found = false;
        for(BankObject_BankAccount book : bankAccounts){
           if (book.getAuthor().equals(author)) {
               found = true;
               System.out.println(book.getBookData());
            }
        }
        if(!found)
            System.out.println("Author was not found");
    }
    
    /****************Searching        BankObject_BankAccount related*************************/
    
//        @Override
//    public void searchAccountData(){
//        String title = prompt.ask("Skriv titeln >");
//        extractAccountDataFromAccountID(findBookIDFromTitle(title));
//    }
    
        @Override
    public int findUserIDFromAccountID(String title){//OK
        int bookID = -1;
        boolean found = false;
        for(BankObject_BankAccount book : bankAccounts){
           if (book.getTitle().equals(title)) {
               bookID = book.getBookID();
               found = true;
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return bookID; 
    }
    
        @Override
    public boolean extractAccountDataFromAccountID(int id){//OK
        boolean found = false;
        if(id != -1)
        {
            for(BankObject_BankAccount book : bankAccounts){
               if (book.getBookID() == id) {
                   found = true;
                   System.out.println("Bokens registrerade uppgifter:\n" + book.getBookData());
                }
            }
        }
        if(!found)
            System.out.println("Boken finns inte");
        
        return found;
    }
         
    
  /**************Searching       User data related**************************/  
    
        @Override
    public void searchUserData(){//OK
        String name = prompt.ask("Skriv låntagarens namn >");
        extractUserDataFromID(findUserIDFromName(name)); 
    }
    
        @Override
    public int findUserIDFromName(String fullName){//OK
        int userID = -1;
        boolean found = false;
        for(BankObject_BankUser user : bankUsers){
           if (user.getName().equals(fullName)) {
               userID = user.getID();
               found = true;
               extractUserDataFromID(userID);
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return userID; 
    }
        @Override
    public int findLoginData(String fullName, String PassWord){  //OK
        System.out.println("Inside findlogin data");
        int userCategoryID = -1;
        boolean found = false;
        for(BankObject_BankUser user : bankUsers)
        {
           if (user.getName().equals(fullName)) 
           {
               System.out.println("User was found " + user.getName());
               if(user.getUserPassWord().equals(PassWord))
               {
                   System.out.println("Password was found: " +user.getUserPassWord());
                   found = true;
                   if(user.category.equals("customer"))
                   {
                       System.out.println("You are a custimer");
                       return 1;
                   }
                   else if(user.category.equals("admin"))
                   {
                       System.out.println("Hallo admin");
                       System.out.println("You are" + user.getName());
                       return 2;
                   }
                }
            }
        }
        
        if(!found)
            System.out.println("User was not found");
        return userCategoryID; 
        
    }
//    public LibrarySession_ findLoginData(String fullName, String PassWord, Object object){
//        int userCategoryID = -1;
//        boolean found = false;
//        for(BankObject_BankUser user : bankUsers){
//           if (user.getName().equals(fullName)) {
//               System.out.println(user.getName());
//               if(user.getUserPassWord().equals(PassWord)){
//                   System.out.println(user.getUserPassWord());
//                   found = true;
//                   if(user.category.equals("customer"))
//                       return new LibrarySession_UserSession(prompt,bankAccounts,bankUsers,bankLoans,null);
//                   else
//                       return new LibrarySession_EmployeeSession(prompt,bankAccounts,bankUsers,bankLoans,null);
//               }
//            }
//        }
//    }
    
        @Override
    public boolean extractUserDataFromID(int id){//OK
        boolean found = false;
        for(BankObject_BankUser user : bankUsers){
           if (user.getID() == id) {
               found = true;
               
               System.out.println("Användarens registrerade uppgifter:\n" + user.getUserData());
               
               
               ArrayList<BankObject_BankLoan> loans = user.getUserLoans();
               if(loans.size() > 0 )
                   System.out.println("Användaren har tidigare registrerade lån\n");
               for (BankObject_BankLoan loan : loans)
                    for(BankObject_BankAccount book : bankAccounts)
                        if(book.getBookID() == loan.bookID)
                            System.out.println(book.getBookData());
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return found;
    }
    
        @Override
    public boolean quitProgram(){//OK
        return true;
    }
}
