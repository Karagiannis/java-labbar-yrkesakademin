/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package banksupporxml;

import java.io.File;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.lang.model.element.Element;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Node;

/**
 *
 * @author Lasse
 */
public class BankSupport {
    
    public static void DeleteNodeFromXMLDoc(Document d, Element e, String fileName) throws TransformerException
    {
        d.removeChild((Node)e);
        WriteDOMtoXMLFile(d, fileName);
        
    }
    
    public static void WriteDOMtoXMLFile(Document d, String fileName) throws TransformerException
    {
        // write the content into xml file
                TransformerFactory transformerFactory = TransformerFactory.newInstance();
                Transformer transformer = null;
        try {
            transformer = transformerFactory.newTransformer();
        } catch (TransformerConfigurationException ex) {
            Logger.getLogger(BankSupport.class.getName()).log(Level.SEVERE, null, ex);
        }

                transformer.setOutputProperty(OutputKeys.INDENT, "yes");
                transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "2");

                DOMSource source = new DOMSource(d);
                StreamResult result = new StreamResult(new File(fileName));
                transformer.transform(source, result);
                // Output to console for testing
                StreamResult consoleResult = new StreamResult(System.out);
                transformer.transform(source, consoleResult);
    }
    
    public static void AppendArticleToArticles(Document d, Node parent,
            String[] keyArray, String[] valueArray )
    {
        org.w3c.dom.Element artikel =  d.createElement("Artikel");
        parent.appendChild((Node) artikel);
        
        for(int i = 0; i < keyArray.length; i++)
        {
             if(i == 0)
             {
                 // write the content into xml file
		Attr attr = d.createAttribute("id");
		attr.setValue(valueArray[i]);
		artikel.setAttributeNode(attr);
             }
             else
             {
                org.w3c.dom.Element element = d.createElement(keyArray[i]);
                Node text = d.createTextNode(valueArray[i]);
                System.out.println(valueArray[i]);
                element.appendChild(text);
                artikel.appendChild(element);
             }
        }
    
    }
    public static void InsertArticleBeforeSiblingInArticles(Document d, Node parent,
            String[] keyArray, String[] valueArray, Node n , String nodename)
    {
        org.w3c.dom.Element artikel =  d.createElement(nodename);
        parent.insertBefore(artikel, n);
        
        for(int i = 0; i < keyArray.length; i++)
        {
             if(i == 0)
             {
                 // write the content into xml file
		Attr attr = d.createAttribute("id");
		attr.setValue(valueArray[i]);
		artikel.setAttributeNode(attr);
             }
             else
             {
                org.w3c.dom.Element element = d.createElement(keyArray[i]);
                Node text = d.createTextNode(valueArray[i]);
                System.out.println(valueArray[i]);
                element.appendChild(text);
                artikel.appendChild(element);
             }
        }
    
    }
    
}
