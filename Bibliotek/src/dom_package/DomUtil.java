/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package dom_package;

import bibliotek.Book;
import bibliotek.LibraryLoan;
import bibliotek.LibraryUser;
import java.util.ArrayList;
import java.util.Collections;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.w3c.dom.Attr;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Lasse
 */
public  class DomUtil {
    
    public static void PrintNodeElements(Node n)
    {
        
        switch(n.getNodeType()) 
        {
            case Node.ELEMENT_NODE:
                Element e = (Element) n;
                System.out.println(e.getTagName());
                ListAllAttributes(e);
                e.getTextContent();
                if (e.hasChildNodes())
                {
                    NodeList list = e.getChildNodes();
                    for (int i = 0; i < list.getLength(); i++)
                        PrintNodeElements(list.item(i));
                }
                break;
            default:
                break;
        }
    }
    public static void PrintDOMElemtTree( Node n, int level)
    {   
        String tabString = "";
        for(int i = 0; i < level; i++)
            tabString += "\t";  
                    
        if(n.getNodeType() == Node.ELEMENT_NODE)
        {
            Element e = (Element) n;
            //System.out.println(tabString + e.getTagName() + " " + getAllAttributes(e) + " Text:" + e.getTextContent());
            System.out.println(tabString + e.getTagName() + " "  + " Text:" + e.getTextContent()); 
            ListAllAttributes(e);
            if (e.hasChildNodes())
            {
                NodeList list = e.getChildNodes();
                level++;
                for (int i = 0; i < list.getLength(); i ++)
                {
                   PrintDOMElemtTree(list.item(i), level);
                }   
            }
        }
    }
    public static void PrintDOMElemtTreeLab1Extra( Node n, int level)
    {   
        String tabString = "";
        for(int i = 0; i < level; i++)
            tabString += "\t";  
                    
        if(n.getNodeType() == Node.ELEMENT_NODE)
        {
            
            Element e = (Element) n;
            //System.out.println(tabString + e.getTagName() + " " + getAllAttributes(e) + " Text:" + e.getTextContent());
            System.out.println(tabString + e.getTagName()); 
            //ListAllAttributes(e);
            if (e.hasChildNodes())
            {
                
                
                NodeList list = e.getChildNodes();
                level++;
                for (int i = 0; i < list.getLength(); i ++)
                {
                   if (list.item(i).getNodeType() == Node.ELEMENT_NODE)    
                    System.out.println(tabString + list.item(i).getNodeName() + "[OPEN]");
                   
                    PrintDOMElemtTreeLab1Extra(list.item(i), level);
                  if (list.item(i).getNodeType() == Node.ELEMENT_NODE) 
                    System.out.println(tabString + list.item(i).getNodeName() + "[CLOSE]");   
                
                }
            }
        }
        else if (n.getNodeType() == Node.TEXT_NODE )
        {
           
            if (!n.getNodeValue().replaceAll("\r", "").replaceAll("\n", "").equals(""))
                System.out.println( tabString + n.getNodeValue().replaceAll("\r", "").replaceAll("\n", ""));
        }
        else if( n.getNodeType() == Node.ATTRIBUTE_NODE)
        {
            System.out.println(tabString + n.getNodeValue());
        }
                
    }
    
    public static void LibraryBooksFillingDataRoutine( Node n, int level, Book book,ArrayList<Book> books)
    {   
        String tabString = "";
        for(int i = 0; i < level; i++)
            tabString += "\t"; 

           
        if(n.getNodeType() == Node.ELEMENT_NODE)
        {  
            //book = new Book();
            Element e = (Element) n;
            //System.out.println(tabString + e.getTagName() + " " + getAllAttributes(e) + " Text:" + e.getTextContent());
            //System.out.println(tabString + e.getTagName()); 
            //ListAllAttributes(e);
            if (e.hasChildNodes())
            {
                               
                NodeList list = e.getChildNodes();
                level++;
                for (int i = 0; i < list.getLength(); i ++)
                {
                   //if (list.item(i).getNodeType() == Node.ELEMENT_NODE)    
                    //System.out.println(tabString + list.item(i).getNodeName() + "[OPEN]");
                                        
                    LibraryBooksFillingDataRoutine(list.item(i), level, book, books);
                    if(e.hasAttributes()){
                        String id = e.getAttribute("id");
                        book.ID = Integer.parseInt(id);
                    }
                      
                    //System.out.println(tabString + list.item(i).getNodeName() + "[CLOSE]"); 
                                   
                }
            }
        }
        else if (n.getNodeType() == Node.TEXT_NODE )
        {
           
            if (!n.getNodeValue().replaceAll("\r", "").replaceAll("\n", "").equals(""))
            {
                String text = n.getNodeValue().replaceAll("\r", "").replaceAll("\n", "");
                 text = n.getNodeValue();
                String parent = n.getParentNode().getNodeName();
                //System.out.println("################################   " + parent);
                    if(parent.equals("author"))
                    {
                        book.author = text;
                        //System.out.println("Setting author!");
                        //System.out.println("book.author " +text);
                    }
                     else if(parent.equals("title")){
                            //System.out.println("setting title!");
                             book.title =  text;
                             //System.out.println("book.title " +text);
                     }
                    else if(parent.equals("genre")){
                            //System.out.println("setting genre!");
                             book.category = text;
                            //System.out.println("book.category " +text);
                     }
                    else if(parent.equals("price")){
                        //System.out.println("setting price!");
                             book.price =  text;
                             //System.out.println("book.price " +text);
                     }
                    else if(parent.equals("publish_date")){
                             book.publish_date = text;
                             //System.out.println("setting publish date!");
                             //System.out.println("book.publish_date " +text);
                     }
                    else if(parent.equals("description")){
                             book.description = text;
                             //System.out.println("setting description!");
                             //System.out.println("book.description " +text);
                     }
                    else if(parent.equals("isavailible")){
                             //System.out.println("setting isavailibe!");
                             book.isAvailible = text.matches("\\btrue\\b")? true: false;
                             //System.out.println("book.isAvailible " +text);
                     }
                    else if(parent.equals("was_loaned_lastime_at")){
                            //System.out.println("setting was_loaned_lastime_a!");
                             book.wasLoanedLastTimeAt = text;
                             //System.out.println("book.wasLoanedLastTimeAt " +text);
                     }
                    else if(parent.equals("loan_expires")){
                         //System.out.println("setting loan expires!");
                             book.loanExpires =  text;
                             //System.out.println("book.loanExpires " +text);
                     }
                    else if(parent.equals("registered_at_date")){
                           //System.out.println("setting registered at date!");
                             book.registeredAtDate = text;
                             //System.out.println(text);
                     }
                    
                    
                   //System.out.println( tabString + text);
                }
                    
                
            
        }
        else if( n.getNodeType() == Node.ATTRIBUTE_NODE)
        {
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("Setting id");
            System.out.println(tabString + n.getNodeValue());
            book.ID = Integer.parseInt(n.getNodeValue());
        }
                
    }
    
public static void LibraryUsersFillingDataRoutine( Node n, int level, LibraryUser user, ArrayList<LibraryUser> libraryUsers)
    {   
        String tabString = "";
        for(int i = 0; i < level; i++)
            tabString += "\t"; 

           
        if(n.getNodeType() == Node.ELEMENT_NODE)
        {  
            //book = new Book();
            Element e = (Element) n;
            //System.out.println(tabString + e.getTagName() + " " + getAllAttributes(e) + " Text:" + e.getTextContent());
            //System.out.println(tabString + e.getTagName()); 
            //ListAllAttributes(e);
            if (e.hasChildNodes())
            {
                               
                NodeList list = e.getChildNodes();
                level++;
                for (int i = 0; i < list.getLength(); i ++)
                {
                   //if (list.item(i).getNodeType() == Node.ELEMENT_NODE)    
                    //System.out.println(tabString + list.item(i).getNodeName() + "[OPEN]");
                                        
                    LibraryUsersFillingDataRoutine(list.item(i), level, user, libraryUsers);
                    if(e.hasAttributes()){
                        String id = e.getAttribute("id");
                        user.userID = Integer.parseInt(id);
                    }
                      
                    //System.out.println(tabString + list.item(i).getNodeName() + "[CLOSE]"); 
                                   
                }
            }
        }
        else if (n.getNodeType() == Node.TEXT_NODE )
        {
           
            if (!n.getNodeValue().replaceAll("\r", "").replaceAll("\n", "").equals(""))
            {
                String text = n.getNodeValue().replaceAll("\r", "").replaceAll("\n", "");
                 text = n.getNodeValue();
                String parent = n.getParentNode().getNodeName();
                //System.out.println("################################   " + parent);
                    if(parent.equals("forenamn"))
                    {
                        user.name = text;
                        //System.out.println("Setting author!");
                        //System.out.println("book.author " +text);
                    }
                     else if(parent.equals("efternamn")){
                            //System.out.println("setting title!");
                             user.familyName = text;
                             //System.out.println("book.title " +text);
                     }
                    else if(parent.equals("personnummer")){
                            //System.out.println("setting genre!");
                             user.personNumber = text;
                            //System.out.println("book.category " +text);
                     }
                    else if(parent.equals("adress")){
                        //System.out.println("setting price!");
                             user.postAdress =  text;
                             //System.out.println("book.price " +text);
                     }
                    else if(parent.equals("postnummer")){
                             user.postNumber = text;
                             //System.out.println("setting publish date!");
                             //System.out.println("book.publish_date " +text);
                     }
                    else if(parent.equals("ort")){
                             user.city = text;
                             //System.out.println("setting description!");
                             //System.out.println("book.description " +text);
                     }
                    else if(parent.equals("email")){
                             //System.out.println("setting isavailibe!");
                             user.email = text;
                             //System.out.println("book.isAvailible " +text);
                     }
                    
                    
                    
                   //System.out.println( tabString + text);
                }
                    
                
            
        }
        else if( n.getNodeType() == Node.ATTRIBUTE_NODE)
        {
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("Setting id");
            System.out.println(tabString + n.getNodeValue());
            user.userID = Integer.parseInt(n.getNodeValue());
        }
                
    }
    
public static void LibraryLoansFillingDataRoutine( Node n, int level, LibraryLoan loan, ArrayList<LibraryLoan> libraryLoans)
    {   
        String tabString = "";
        for(int i = 0; i < level; i++)
            tabString += "\t"; 

           
        if(n.getNodeType() == Node.ELEMENT_NODE)
        {  
            //book = new Book();
            Element e = (Element) n;
            //System.out.println(tabString + e.getTagName() + " " + getAllAttributes(e) + " Text:" + e.getTextContent());
            //System.out.println(tabString + e.getTagName()); 
            //ListAllAttributes(e);
            if (e.hasChildNodes())
            {
                               
                NodeList list = e.getChildNodes();
                level++;
                for (int i = 0; i < list.getLength(); i ++)
                {
                   //if (list.item(i).getNodeType() == Node.ELEMENT_NODE)    
                    //System.out.println(tabString + list.item(i).getNodeName() + "[OPEN]");
                                        
                    LibraryLoansFillingDataRoutine(list.item(i), level, loan, libraryLoans);
                    if(e.hasAttributes()){
                        String id = e.getAttribute("id");
                        loan.loanID = Integer.parseInt(id);
                    }
                      
                    //System.out.println(tabString + list.item(i).getNodeName() + "[CLOSE]"); 
                                   
                }
            }
        }
        else if (n.getNodeType() == Node.TEXT_NODE )
        {
           
            if (!n.getNodeValue().replaceAll("\r", "").replaceAll("\n", "").equals(""))
            {
                String text = n.getNodeValue().replaceAll("\r", "").replaceAll("\n", "");
                 text = n.getNodeValue();
                String parent = n.getParentNode().getNodeName();
                //System.out.println("################################   " + parent);
                    if(parent.equals("media_id"))
                    {
                        loan.bookID = Integer.parseInt(text);
                        //System.out.println("Setting author!");
                        //System.out.println("book.author " +text);
                    }
                     else if(parent.equals("user_id")){
                            //System.out.println("setting title!");
                             loan.userID = Integer.parseInt(text);
                             //System.out.println("book.title " +text);
                     }
                    else if(parent.equals("loan_date")){
                            //System.out.println("setting genre!");
                             loan.loanDate = text;
                            //System.out.println("book.category " +text);
                     }
                    else if(parent.equals("expires_date")){
                        //System.out.println("setting price!");
                             loan.expiresDate =  text;
                             //System.out.println("book.price " +text);
                     }
                   //System.out.println( tabString + text);
                }
                    
                
            
        }
        else if( n.getNodeType() == Node.ATTRIBUTE_NODE)
        {
            System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            System.out.println("Setting id");
            System.out.println(tabString + n.getNodeValue());
            loan.loanID = Integer.parseInt(n.getNodeValue());
        }
                
    }
    
    
    public static void AnalyzeDomTreeLab1Extra(Node n, int[] content, int level)
    {
        int arrayIndexForNmberOfElements = 0;
        int arrayIndexForNumberOfAttributes = 1;
        int arrayIndexForNumberOfComments = 2;
        int arrayIndexForNumberOfTextNodes = 3;
        
        Pattern pBreak = Pattern.compile("\\n");
        Pattern pSpace = Pattern.compile("\\s");
        Pattern pAny = Pattern.compile("\\S");
        
        
        
        switch(n.getNodeType())
        {
            case Node.ELEMENT_NODE:
            {
                System.out.println("NodeName: " + n.getNodeName());
                System.out.println("NodeValue: " + n.getNodeValue());
                Element e = (Element )n;
                System.out.println("Node base URI: " + n.getBaseURI());
                System.out.println("Tag name for element cast: " + e.getTagName());
                System.out.println("node text content: " + n.getTextContent());
                System.out.println("Element cast text content: "+ e.getTextContent());
                System.out.println("ElementhasChildNodes: " + String.valueOf(n.hasChildNodes()));
                System.out.println("NumberOfChild Nodes: " + e.getChildNodes().getLength());
                content[arrayIndexForNmberOfElements] +=1;
                if (n.hasAttributes())
                {
                    NamedNodeMap attributes = n.getAttributes();
                    int numAttrs = attributes.getLength();
                    content[arrayIndexForNumberOfAttributes] += numAttrs;
                }
                break;
            }
            case Node.TEXT_NODE:
            {
                System.out.println("Node base URI: " + n.getBaseURI());
                System.out.println("child nodeValue: " + n.getNodeValue()+
                        ExtractRegExpMatchedContent(pBreak,n.getNodeValue(), "\\n" )+ "...." +
                        ExtractRegExpMatchedContent(pSpace,n.getNodeValue(), "\\s" )+ "...." +
                        ExtractRegExpMatchedContent(pAny,n.getNodeValue(), null ));
                content[arrayIndexForNumberOfTextNodes] += 1;
                break;
            
            }
            case Node.COMMENT_NODE:
            {
                content[arrayIndexForNumberOfComments] += 1;
                break;
            }
            case Node.ATTRIBUTE_NODE:
            {
                Attr attr = (Attr) n;
                String attrName = attr.getNodeName();
	        String attrValue = attr.getNodeValue();
                String attrText = attr.getTextContent();
                String attrBaseURI = attr.getBaseURI();
	        System.out.println("Found attribute: " + attrName + " with value: " + attrValue +
                        " with text content " + attrText + " with base URI " + attrBaseURI);
                
            }
            default:
                break;
                
        }//End switch
        //System.arraycopy(temp, 0, content, 0, 3);
        if (n.hasChildNodes())
        {
            NodeList nList = n.getChildNodes();
            int len = nList.getLength();
            for(int index = 0; index < len; index++)
            {
                Node child = nList.item(index);
                System.out.println("********Before put into recursion **************");
                System.out.println("child base URI: " + child.getBaseURI());
                System.out.println("child locale name: " + child.getLocalName());
                System.out.println("child namespaceURI: " + child.getNamespaceURI());
                System.out.println("child nodeName: " + child.getNodeName());
                System.out.println("child prefix: " + child.getPrefix());
                System.out.println("child TextContent: " + child.getTextContent());
                System.out.println("child toString: " + child.toString());
                System.out.println("child hasAttributes: " + String.valueOf(child.hasAttributes()));
                System.out.println("child hasChildNodes: " + String.valueOf(child.hasChildNodes()));
                for (int j = 0; j < 4; j++)
                    System.out.println("Content i" +j + "  "+ content[j]);
                System.out.println("Level before descent: " + level);
                System.out.println("********Is put into recursion now**************");
                
                AnalyzeDomTreeLab1Extra(child, content, ++level);
                
                System.out.println("Popping back from level " + level);
                for (int i = 0; i < 4; i++)
                    System.out.println("Content i: " +i + "  "+ content[i]);
                System.out.println("index: " + index);
                System.out.println("Popping back final");  
            }//End for loop;
        }
    }
    public static String ExtractRegExpMatchedContent( Pattern p, String target,String indicate)
    {
            Matcher m = p.matcher(target);
            String str = "";
            if (indicate != null)
            {
                while(m.find())
                {
                    str += indicate;
                }
            }
            else
            {
                while(m.find())
                {
                    str += "..." +  m.group() + "....";
                }
            }
            return str;
        
    }
   
    public static Element getFirstElementChild(NodeList nList)
    {
        Element e = null;
        
        for (int i = 0; i < nList.getLength(); i ++)
        {
            Node n = nList.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE)
            { 
                e = (Element) n;
                break;
            }
        }
        return e;
    }
    public static Element getFirstElementChild(Element element)
    {
        NodeList nList = element.getChildNodes();
        Element e = null;
        
        for (int i = 0; i < nList.getLength(); i ++)
        {
            Node n = nList.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE)
            { 
                e = (Element) n;
                break;
            }
        }
        return e;
    }
    
    public static ArrayList<Element> getChildElements(NodeList nList)
    {
        Element e = null;
        ArrayList<Element> elements = new ArrayList<Element>();
        for (int i = 0; i < nList.getLength(); i++)
        {
            Node n = nList.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE)
            {
                e = (Element) n;
                elements.add(e);
            }
        
        }
        return elements;
    }
    public static ArrayList<Element> getChildElements(Element element)
    {
        NodeList nList = element.getChildNodes();
        Element e = null;
        ArrayList<Element> elements = new ArrayList<Element>();
        for (int i = 0; i < nList.getLength(); i++)
        {
            Node n = nList.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE)
            {
                e = (Element) n;
                elements.add(e);
            }
        
        }
        return elements;
    }
    
    public static Element getNearestSiblingElement(Element element)
    {
        Node n = element.getNextSibling();
        Element e = null;
        boolean found = false;
        while(n.getNodeType()!= Node.ELEMENT_NODE && !found)
        {
            if (n.getNodeType() == Node.ELEMENT_NODE)
            {
                found = true;
                e = (Element) n;
                return e;
            }
            else
            {
                n = n.getNextSibling();
            }
        }
        if(n.getNodeType() == Node.ELEMENT_NODE)
            return (Element)n ;
        else
            return null;
        
    }
    
    
    public static Element getLastElementChild(NodeList nList)
    {
        Element e = null;
        
        for (int i = nList.getLength()-1; i >= 0; i--)
        {
            Node n = nList.item(i);
            if (n.getNodeType() == Node.ELEMENT_NODE)
            { 
                e = (Element) n;
                break;
            }
        }
        return e;
    }
    
    public static void AnalyzeDomTreeLab1ExtraSecond(Element e)
    {
        //Print out Leaf nodes Only
        PrintOutLeafElementNodes(e);
        System.out.println("*********LAST PARENTLEVEL**********");
        PrintOutLastParentLevel(e);
    }
    public static void PrintOutLastParentLevel(Element element)
    {
          if (getFirstElementChild(element) == null)//last childlevel is found
          {
              Node parent = element.getParentNode();
              System.out.println(parent.getTextContent());
              ListAllAttributes((Element) parent);
              PrintOutLeafElementNodes((Element)parent); 
              Element sibling = getNearestSiblingElement((Element)parent);
              ListAllAttributes((Element) sibling);
              PrintOutLeafElementNodes((Element) sibling); 
          }
          else
          {
                PrintOutLastParentLevel(getFirstElementChild(element)); 
        }
              
    }
    
    public static String[] ReturnSubStrings(String s)
    {
        Pattern p = Pattern.compile("[A-Z]");
        Matcher m = p.matcher(s);
        ArrayList<Integer> indexes;
        indexes = new ArrayList<Integer>();
        while (m.find())
        {
            indexes.add(m.start());
        }
        p = Pattern.compile("[0-9]{1}");
        m = p.matcher(s);
        int index = m.start();
        String[] strArray = new String[indexes.size() +1];
        int i = 0;
        for (i = 0; i < strArray.length -1; i++)
        {
            strArray[i] = s.substring(indexes.get(i), indexes.get(i+1));
        }
        strArray[i]=s.substring(i+1);
        return strArray;
    
    }
    
    public static void PrintOutLeafElementNodes(Element e)
    {
        if (getFirstElementChild(e) == null)//this is a leafnode
        {
            System.out.println(e.getTagName()+ "  "+ e.getTextContent());
        }
        else
        {
            ArrayList<Element> elements = new ArrayList<Element>();
            elements = getChildElements(e);
            for(int i = 0; i < elements.size(); i++)
            {
                PrintOutLeafElementNodes(elements.get(i));
            }
        }
    }
    
    public static void ListAllAttributes(Element element) 
    {
	        System.out.println("List attributes for node: " + element.getNodeName());
	         // get a map containing the attributes of this node
	        NamedNodeMap attributes = element.getAttributes();
	        // get the number of nodes in this map
	        int numAttrs = attributes.getLength(); 
	        for (int i = 0; i < numAttrs; i++) 
                {
	            Attr attr = (Attr) attributes.item(i);
                    String attrName = attr.getNodeName();
	            String attrValue = attr.getNodeValue();
	            System.out.println("Found attribute: " + attrName + " with value: " + attrValue);
	        }
    }
    
    
    public static String getAllAttributes(Element element)
    {
        String allAttributesAsOneString = "";
        NamedNodeMap attributes = element.getAttributes();
        int numAttrs = attributes.getLength(); 
	        for (int i = 0; i < numAttrs; i++) 
                {
	            Attr attr = (Attr) attributes.item(i);
                    String attrName = attr.getNodeName();
	            String attrValue = attr.getNodeValue();
                    allAttributesAsOneString = attrName +" : " + attrValue;
	            
	        }
        return allAttributesAsOneString;
    }
    
}
