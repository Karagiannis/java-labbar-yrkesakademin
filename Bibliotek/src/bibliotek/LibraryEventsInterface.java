/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

/**
 *
 * @author Lasse
 */
public interface LibraryEventsInterface {

    int WelcomeString();

    /*******************Registration       book dat
     * @return a**************************/
    Book registerABook();

    /******************** Registation        bookloan *************************/
    void registerABookLoan();

    /****************************************************************/
    /************************* Registration events******************/
    void registerABookReturn();

    /****************************  Registration  user data******************************/
    void registerAUser();
    
   

    /**********************************************************************/
    /***********************Searching**************************************/
    void search();
    
    void searchAuthorData();

    /****************Searching        Book related****************************/
    void searchTitleData();

    int findBookIDFromTitle(String title);

    boolean extractBookDataFrombookID(int id);

    /**************Searching       User data related**************************/
    void searchUserData();

    int findUserIDFromName(String fullName);

    boolean extractUserDataFromID(int id);
    
    boolean quitProgram();
    
    
}
