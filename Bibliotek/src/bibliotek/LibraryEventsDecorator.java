/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public abstract class LibraryEventsDecorator implements LibraryEventsInterface {
    
    LibraryEventsInterface libEvents;
    
    
    public ArrayList<Book> books;
    public ArrayList<LibraryUser> libraryUsers;
    protected final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected  Prompt prompt;

    public LibraryEventsDecorator( LibraryEventsInterface libEvents) {
        this.libEvents = libEvents;
    }

    @Override
    public int WelcomeString(){
        return libEvents.WelcomeString();
    }
    /****************************************************************/
    /************************* Registration events******************/
    @Override
    public  void registerABookReturn(){
        libEvents.registerABookReturn();
    }
    /******************** Registation        bookloan *************************/
    @Override
    public void registerABookLoan(){
        libEvents.registerABookLoan();
    }
    /*******************Registration       book data**************************/
    @Override
    public Book registerABook(){
    
        return libEvents.registerABook();
    }

    /****************************  Registration  user data******************************/
    @Override
    public void registerAUser(){
        libEvents.registerAUser();
    }

   

    /**********************************************************************/
    /***********************Searching**************************************/
   @Override
    public  void search(){
        libEvents.search();
    }

    /********Utility method with pattern matchin not currently used**********/
    
    

    /*************Searching      Author data related************************/
    @Override
    public  void searchAuthorData(){
        libEvents.searchAuthorData();
    }

    /****************Searching        Book related****************************/
    public void searchTitleData(){
        libEvents.searchTitleData();
    }

    public  int findBookIDFromTitle(String title){
    
        return libEvents.findBookIDFromTitle(title);
    }

    public boolean extractBookDataFrombookID(int id){
    
        return libEvents.extractBookDataFrombookID(id);
    }

    /**************Searching       User data related**************************/
    public void searchUserData(){
        libEvents.searchUserData();
    
    }

    public  int findUserIDFromName(String fullName){
        return libEvents.findUserIDFromName(fullName);
    }

    public  boolean extractUserDataFromID(int id){
        return libEvents.extractUserDataFromID(id);
    }
    public  boolean quitProgram(){
        return libEvents.quitProgram();
    }
    
}
