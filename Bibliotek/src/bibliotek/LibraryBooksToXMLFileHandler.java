/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

import dom_package.DomUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;

/**
 *
 * @author Lasse
 */
public class LibraryBooksToXMLFileHandler extends LibraryEventsDecorator {
    
    LibraryEventsInterface libEvents;
    ArrayList<Book> books;
    ArrayList<LibraryUser> libraryUsers;
    ArrayList<LibraryLoan> libraryLoans;
    String filePathBooks = "c:\\LOG\\books.xml";
    String filePathUsers = "c:\\LOG\\libraryUsers.xml";
    String filePathLibraryLoans = "c:\\LOG\\libraryLoans.xml";
    
    public LibraryBooksToXMLFileHandler(LibraryEventsInterface libEvents,
                                         ArrayList<Book> books,
                                         ArrayList<LibraryUser> libraryUsers,
                                         ArrayList<LibraryLoan> libraryLoans)
    {
            super(libEvents);
            this.libEvents = libEvents;
            this.books = books;
            this.libraryUsers = libraryUsers;
            this.libraryLoans = libraryLoans;
            
            //Transfer data from xml-file to datastructure books, every time program is started
           loadBooksFromXMLFileToDataStructure();
           
           //Transfer data from xml-file to datastructure users, every time program is started
           loadUsersFromXMLFileToDataStructure();
           
           //Transfer data from xml-file to datastructure loans, every time program is started
           loadLoansFromXMLFileToDataStructure();
    }
    
    private void loadBooksFromXMLFileToDataStructure(){
        
        //Populate datastructure libraryBooks
            try{
                    File fXmlFile = new File(filePathBooks);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(new FileInputStream(fXmlFile));
                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    //NodeList nList = doc.getChildNodes();
                    Element e =  doc.getDocumentElement();
                    //Element first = DomUtil.getFirstElementChild(e);
                    ArrayList<Element> elemArray = DomUtil.getChildElements(e);
                    System.out.println(e.getNodeName() + "  [OPEN]");
                        for (int i = 0; i < elemArray.size(); i++)
                        {
                            System.out.println("Size "+ elemArray.size());
                           
                            Book b = new Book();
                            //if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                           
                            DomUtil.LibraryBooksFillingDataRoutine(elemArray.get(i), 1, b, books);
                            books.add(b);
                        }
                    System.out.println(e.getNodeName() + "  [CLOSE]");

                
                
                }catch (Exception e) 
                {
                     System.out.println("Exception thrown  now :" + e);
                } 
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            for(Book book : books)
               System.out.println( book.getBookData());
        
    }
     
    
    private void loadUsersFromXMLFileToDataStructure(){
        
        //Populate datastructure libraryUsers
            try{
                    File fXmlFile = new File(filePathUsers);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    //NodeList nList = doc.getChildNodes();
                    Element e =  doc.getDocumentElement();
                    //Element first = DomUtil.getFirstElementChild(e);
                    ArrayList<Element> elemArray = DomUtil.getChildElements(e);
                    System.out.println(e.getNodeName() + "  [OPEN]");
                        for (int i = 0; i < elemArray.size(); i++)
                        {
                            System.out.println("Size "+ elemArray.size());
                           
                            LibraryUser b = new LibraryUser();
                            //if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                           
                            DomUtil.LibraryUsersFillingDataRoutine(elemArray.get(i), 1, b, libraryUsers);
                            libraryUsers.add(b);
                        }
                    System.out.println(e.getNodeName() + "  [CLOSE]");
                }catch (Exception e) 
                {
                     System.out.println("Exception thrown  now :" + e);
                } 
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            for(LibraryUser user : libraryUsers)
               System.out.println( user.getUserData());
        
    }
    
    private void loadLoansFromXMLFileToDataStructure(){
    
    
        //Populate datastructure libraryLoans
            try{
                    File fXmlFile = new File(filePathLibraryLoans);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    //NodeList nList = doc.getChildNodes();
                    Element e =  doc.getDocumentElement();
                    //Element first = DomUtil.getFirstElementChild(e);
                    ArrayList<Element> elemArray = DomUtil.getChildElements(e);
                    System.out.println(e.getNodeName() + "  [OPEN]");
                        for (int i = 0; i < elemArray.size(); i++)
                        {
                            System.out.println("Size "+ elemArray.size());
                           
                            LibraryLoan b = new LibraryLoan();
                            //if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                           
                            DomUtil.LibraryLoansFillingDataRoutine(elemArray.get(i), 1, b, libraryLoans);
                            libraryLoans.add(b);
                        }
                    System.out.println(e.getNodeName() + "  [CLOSE]");
                }catch (Exception e) 
                {
                     System.out.println("Exception thrown  now :" + e);
                } 
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            for(LibraryLoan loan : libraryLoans)
               System.out.println( loan.getLoanData());
    
    }
    
    @Override
    public int WelcomeString(){
        return super.WelcomeString();
    }
    /****************************************************************/
    /************************* Registration events******************/
    @Override
    public  void registerABookReturn(){
        super.registerABookReturn();
    }
    /******************** Registation        bookloan *************************/
    @Override
    public void registerABookLoan(){
        super.registerABookLoan();
    }
    /*******************Registration       book dat
     * @return a**************************/
    @Override
    public Book registerABook(){
    
        Book b = super.registerABook();
        if(b != null){
            try{
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();      
                    Document document = documentBuilder.parse(filePathBooks);
                    Element root = document.getDocumentElement();
                    Element newBook = document.createElement("book");
                    Attr id_Attr = document.createAttribute("id");
                    String id = Integer.toString(b.ID);
                    id_Attr.setValue(id);
                    newBook.setAttributeNode(id_Attr);
                    root.appendChild(newBook);

                     Element tagName = document.createElement("author");
                                    tagName.appendChild(document.createTextNode(b.getAuthor()));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("title");
                                    tagName.appendChild(document.createTextNode(b.getTitle()));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("genre");
                                    tagName.appendChild(document.createTextNode(b.category));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("price");
                                    tagName.appendChild(document.createTextNode(b.price));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("publish_date");
                                    tagName.appendChild(document.createTextNode(b.publish_date));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("description");
                                    tagName.appendChild(document.createTextNode(b.description));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("isavailible");
                                    tagName.appendChild(document.createTextNode("true"));
                                    newBook.appendChild(tagName);

                    tagName = document.createElement("was_loaned_lastime_at");
                                    tagName.appendChild(document.createTextNode(b.wasLoanedLastTimeAt));
                                    newBook.appendChild(tagName); 
                                    
                    tagName = document.createElement("loan_expires");
                                    tagName.appendChild(document.createTextNode(b.loanExpires));
                                    newBook.appendChild(tagName); 
                    
                    tagName = document.createElement("registered_at_date");
                                    tagName.appendChild(document.createTextNode(b.registeredAtDate));
                                    newBook.appendChild(tagName); 
                                  
                                         

                    DOMSource source = new DOMSource(document);
                    TransformerFactory transformerFactory = TransformerFactory.newInstance();
                    Transformer transformer = transformerFactory.newTransformer(); 
                    StreamResult result = new StreamResult(filePathBooks);
                    transformer.transform(source, result);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        return b;
    }
    
    

    /****************************  Registration  user data******************************/
    @Override
    public void registerAUser(){
        super.registerAUser();
    }

   

    /**********************************************************************/
    /***********************Searching**************************************/
   @Override
    public  void search(){
        super.search();
    }

    /********Utility method with pattern matchin not currently used**********/
    
    

    /*************Searching      Author data related************************/
    @Override
    public  void searchAuthorData(){
        super.searchAuthorData();
    }

    /****************Searching        Book related****************************/
    @Override
    public void searchTitleData(){
        super.searchTitleData();
    }

    @Override
    public  int findBookIDFromTitle(String title){
    
        return super.findBookIDFromTitle(title);
    }

    @Override
    public boolean extractBookDataFrombookID(int id){
    
        return super.extractBookDataFrombookID(id);
    }

    /**************Searching       User data related**************************/
    @Override
    public void searchUserData(){
        super.searchUserData();
    
    }

    @Override
    public  int findUserIDFromName(String fullName){
        return super.findUserIDFromName(fullName);
    }

    @Override
    public  boolean extractUserDataFromID(int id){
        return super.extractUserDataFromID(id);
    }
    
    @Override
    public boolean quitProgram(){
        return super.quitProgram();
    }
    
}
