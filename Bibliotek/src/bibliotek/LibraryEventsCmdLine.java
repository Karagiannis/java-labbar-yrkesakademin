/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Lasse
 */
public class LibraryEventsCmdLine implements LibraryEventsInterface {
    
        ArrayList<Book> books;
        ArrayList<LibraryUser> libraryUsers;
        ArrayList<LibraryLoan> libraryLoans;
        Prompt prompt;
        private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    
    public LibraryEventsCmdLine(ArrayList<Book> books, ArrayList<LibraryUser> libraryUsers,
                                     ArrayList<LibraryLoan> libraryLoans, Prompt prompt){
        
        
        this.books = books;
        this.libraryUsers = libraryUsers;
        this.libraryLoans = libraryLoans;
        this.prompt = prompt;
    }
    
    @Override
    public int WelcomeString(){
    String question = "1 - Registrera bok\n"+
               "2 - Registrera låntagare\n" +
               "3 - Registrera lån \n" +
               "4 - Registrera återlämning\n" +
               "5 - Sök \n"     +
               "6 - Avsluta \n" +
               "Vad vill du göra? >";
               
    int inputCode;
       try{
            inputCode = Integer.parseInt(prompt.ask(question));
          }catch(Exception e){
              System.out.println("Måste ange siffrorna 1 till 6");
              System.out.println(e);
              return -1;
              
          }
      return inputCode;  
    }
    
   /****************************************************************/
    /************************* Registration events******************/
    
    /**************REgister a book return**************************/
    @Override
    public void registerABookReturn(){
        String userID = prompt.ask("Låntagarens ID: ");
        int iUserID = Integer.parseInt(userID);
        
        String bookID = prompt.ask("Bokens ID: ");
        int ibookID  = Integer.parseInt(userID);
        
        for(LibraryUser libraryUser : libraryUsers){
            if(iUserID == libraryUser.userID){
                libraryUser.unregisterALoan(ibookID);
            }       
        }
    
    }
    /******************** Registation        bookloan *************************/
    @Override
    public void registerABookLoan(){
        String userID = prompt.ask("Låntagarens ID: ");
        int iUserID = Integer.parseInt(userID);
        
        String bookID = prompt.ask("Bokens ID: ");
        int ibookID  = Integer.parseInt(userID);
        
        for(LibraryUser libraryUser : libraryUsers){
            if(iUserID == libraryUser.userID){
                for(Book book : books){
                    if (book.getBookID() == ibookID)
                        if(book.isAvailibe()){
                            System.out.println("Boken finns tillgänglig");
                            libraryUser.registerALoan(book);
                            String loanExpires = book.loan();
                            book.setAsUnAvailible();
                            System.out.println("Lånet har registrerats");
                            System.out.println(loanExpires);
                           
                            break;
                        }
                        else{
                            System.out.println("Lånet kan inte registreras");
                        }
                }
             break;
            } 
            else{
                System.out.println("Användaren finns inte i databasen");
            }
        }
    }
    /*******************Registration       book data**************************/
    @Override
    public Book registerABook(){
        
    Book book = null;   
        
     String title = prompt.ask("Bokens titel: ");
     String author = prompt.ask("Bokens författare (Efternamn, Namn): ");
     String availibleCategories =   "Välj kategori\n" +
                                    "1 - Teknik\n"+
                                    "2 - Skönlitteratur\n" +
                                    "3 - Tdining \n" +
                                    "4 - CD/DVD\n" ;
     
     
     String categoryIdString = prompt.ask(availibleCategories);
     String category = "";
     int categoryId;
     try{
        
        categoryId = Integer.parseInt(categoryIdString);
        switch (categoryId){

            case 1:
                category = "Teknik";
                break;
            case 2:
                category = "Skönlitteratur";
                break;
            case 3:
                category = "Tidning";
                break;
            case 4:
                category = "CD/DVD";
                break;

        }
     }catch(Exception e){
              System.out.println("Måste ange siffrorna 1 till 4");
              System.out.println(e);
              return book;    
          }
     
     String price = prompt.ask("Pris");
     String publishDate = prompt.ask("Publiceringsdatum ÅÅÅÅ-MM-DD: ");
     String description = prompt.ask("Beskrivning: ");
     Boolean isAvailible = true;
     String wasLoanedLastTime = " 1970-01-01";
     String loanExpires = " 1970-01-01";
     String today =  getTodaysDate();
     book = new Book( books.size(), title, author, category );
     book.price = price;
     book.publish_date = publishDate;
     book.description = description;
     book.isAvailible = true;
     book.wasLoanedLastTimeAt = wasLoanedLastTime;
     book.loanExpires = loanExpires;
     book.registeredAtDate = today;
     
     
     
     books.add(book);
     System.out.println("You registered book ID: " + books.size() );
     System.out.println("title: " + title);
     System.out.println("author: " + author);
     System.out.println("category: " + category );
     
      return book;    
    }
     /****************************  Registration  user data******************************/
    @Override
    public void registerAUser(){
        
        String name = prompt.ask("Ny låntagares, förnamn: >");
        String familyName = prompt.ask("Ny låntagares, efternamn: >");
        String personNumber = prompt.ask("Ny låntagares, personnummer (yy-mm-dd-xxxx) >");
        String postAdress = prompt.ask("Ny låntagares, post-adress: >");
        String postNumber = prompt.ask("Ny låntagares, postnummer : >");
        String city = prompt.ask("Ny låntagares, ort: >");
        String email = prompt.ask("Ny låntagares, e-mail adress: >");
        
        int numberIDOfNewUser = libraryUsers.size();
        LibraryUser user = new LibraryUser(numberIDOfNewUser, name, familyName);
        user.personNumber = personNumber;
        user.postAdress = postAdress;
        user.postNumber = postNumber;
        user.city = city;
        user.email = email;
        
        libraryUsers.add(user);
        
        System.out.println("Du registrerade användar ID: " + numberIDOfNewUser );
        System.out.println("förnamn: " + name);
        System.out.println("efternamn: " + familyName);

        
    }
     private String getTodaysDate() {
        return dateFormat.format(new Date());
    }
    
     /**********************************************************************/
     /***********************Searching**************************************/
    @Override
    public void search()
    {
        String searchCategories =   "1 - Titel (bok, cd/dvd, tidning)\n"+
                                    "2 - Författare\n" +
                                    "3 - Låntagar info \n" +
                                    "4 - Avsluta sökning\n" +
                                    "Vad vill du söka? >";
        int categoryId;
      do{
            String categoryIdString = prompt.ask(searchCategories);
            categoryId = Integer.parseInt(categoryIdString);
            switch (categoryId)
            {
                case 1:
                    searchTitleData();
                    break;
                case 2:
                    searchAuthorData();
                    break;
                case 3:
                    searchUserData();
                    break;
                case 4:
                    break; 
            }       
        }while(categoryId != 4);
    }
    
    /********Utility method with pattern matchin not currently used**********/
    private boolean matchString(String input, String expression){
         String REGEX = expression;
         String INPUT = input;
                                    
   
       Pattern p = Pattern.compile(REGEX);
       Matcher m = p.matcher(INPUT); // get a matcher object
     
       if(m.find()) {
         System.out.println("Found match");
         return true;
       }
       else return false;
    }
    
    
    /*************Searching      Author data related************************/
    public void searchAuthorData(){
        String author = prompt.ask("Skriv författarens namn >");
        boolean found = false;
        for(Book book : books){
           if (book.getAuthor().equals(author)) {
               found = true;
               System.out.println(book.getBookData());
            }
        }
        if(!found)
            System.out.println("Author was not found");
    }
    
    /****************Searching        Book related****************************/
    
    public void searchTitleData(){
        String title = prompt.ask("Skriv titeln >");
        extractBookDataFrombookID(findBookIDFromTitle(title));
    }
    
    public int findBookIDFromTitle(String title){
        int bookID = -1;
        boolean found = false;
        for(Book book : books){
           if (book.getTitle().equals(title)) {
               bookID = book.getBookID();
               found = true;
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return bookID; 
    }
    
    public boolean extractBookDataFrombookID(int id){
        boolean found = false;
        if(id != -1)
        {
            for(Book book : books){
               if (book.getBookID() == id) {
                   found = true;
                   System.out.println("Bokens registrerade uppgifter:\n" + book.getBookData());
                }
            }
        }
        if(!found)
            System.out.println("Boken finns inte");
        
        return found;
    }
         
    
  /**************Searching       User data related**************************/  
    
    public void searchUserData(){
        String name = prompt.ask("Skriv låntagarens namn >");
        extractUserDataFromID(findUserIDFromName(name)); 
    }
    
    public int findUserIDFromName(String fullName){
        int userID = -1;
        boolean found = false;
        for(LibraryUser user : libraryUsers){
           if (user.getName().equals(fullName)) {
               userID = user.getID();
               found = true;
               extractUserDataFromID(userID);
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return userID; 
    }
    
    
    public boolean extractUserDataFromID(int id){
        boolean found = false;
        for(LibraryUser user : libraryUsers){
           if (user.getID() == id) {
               found = true;
               
               System.out.println("Användarens registrerade uppgifter:\n" + user.getUserData());
               
               
               ArrayList<LibraryLoan> loans = user.getUserLoans();
               if(loans.size() > 0 )
                   System.out.println("Användaren har tidigare registrerade lån\n");
               for (LibraryLoan loan : loans)
                    for(Book book : books)
                        if(book.getBookID() == loan.bookID)
                            System.out.println(book.getBookData());
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return found;
    }
    
        @Override
    public boolean quitProgram(){
        return true;
    }
}
