/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

/**
 *
 * @author Lasse
 */
public class LibraryLoan {
    
    public int bookID;
    public int loanID;
    public int userID;
    public String loanDate;
    public String expiresDate;
   
  
    
    public LibraryLoan(int bookID){
        this.bookID = bookID;
    }
    
    public LibraryLoan(){
    }
    
    public LibraryLoan(Book b){
        bookID = b.getBookID();
    }
    
   public LibraryLoan getBookLoan(int bookID){
        if (bookID == this.bookID)
            return this;
        else
            return null;
    }
   public int getBookIDFromBookLoan(){
       return this.bookID;
   }
  public String getLoanData(){
  
      String s = String.format("Loan ID: %d\nUser ID: %d\nMedia ID: %d\nLånedaturm : %s\nLånet förfaller: %s\n",
              loanID,userID, bookID,loanDate,expiresDate);
      return s;
   
  }
}
