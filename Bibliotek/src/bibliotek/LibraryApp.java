/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

import java.util.ArrayList;
import java.util.Scanner;

/**
 *
 * @author Lasse
 */
public class LibraryApp {

    /**
     * @param args the command line arguments
     */
    Prompt prompt = new Prompt();
    ArrayList<Book> books = new ArrayList<>();
    ArrayList<LibraryUser> libraryUsers = new ArrayList<>();
    ArrayList<LibraryLoan> libraryLoans = new ArrayList<>();
    //LibraryEventsCmdLine events = new LibraryEventsCmdLine(books,libraryUsers, prompt);
    LibraryBooksToXMLFileHandler events =
            new LibraryBooksToXMLFileHandler(new LibraryEventsCmdLine(books,libraryUsers,libraryLoans, prompt),
                                              books, libraryUsers, libraryLoans);
    
    public static void main(String[] args) {
        // TODO code application logic here
        LibraryApp library = new LibraryApp();
        library.run();
        
        
    }
    
    private void run(){
    
     
        while(true)
        {
                int command = (events.WelcomeString());

                switch(command){
                    case 1:
                        events.registerABook();
                        break;
                    case 2:
                        events.registerAUser();
                        break;
                    case 3:
                        events.registerABookLoan();
                        break;
                    case 4:
                        events.registerABookReturn();
                        break;
                    case 5:  
                        events.search();
                        break;
                    case 6:
                        events.quitProgram();
                        return;
                  
                }           
        }   
    }   
}
