/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Lasse
 */
public class Book {
    public int ID = 0;
    public String title = "";
    public String category = "";
    public String author = "";
    public String price = "";
    public String publish_date;
    public String description;
    public boolean isAvailible = true;
    public String wasLoanedLastTimeAt = "1970-01-01";
    public String loanExpires = "1970-01-01";
    public String registeredAtDate;
    private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
    
    public Book(int ID, String title, String author, String category ){
        this.ID = ID;
        this.title = title;
        this.author = author;
        this.category = category;
        registeredAtDate = getDateOfToday();
        
    }
    
    public Book(){}
    
    public int getBookID(){
        return this.ID;
    }
    
    public String getTitle(){
        return this.title;
    }
     
    public String getAuthor(){
        return this.author;
    }
    
    public String getBookData(){
        String data1 = " ID: "+ Integer.toString(ID) +"\n";
        String data2 = "Titel:" + title +"\n";
        String data3 = "Genre: " + category +"\n";
        String data4 = "Författare/Upphovsman:" + author+"\n";
        String data5 = "Inköpspris:"+ price +"\n";
        String data6 = "Utgivningsdatum: " +publish_date +"\n";
        String data7 = "Beskrivning: " + description +"\n";
        String data8 = "Tillgänglig:";
        String data9 = (isAvailible)? "sant\n" : "falskt\n";
        String data10 = "Lånades senast:" + wasLoanedLastTimeAt+"\n";
        String data11 = "Förfallsdaturm: "+loanExpires +"\n";
        String data12 = "Media registreardes datum efter inköp:" + registeredAtDate;
   
        
       return data1 + data2 + data3 + data4 + data5 + data6 + data7 + data8 + data9 + data10 + data11 +data12;
    }
    
    public String loan(){
        this.isAvailible = false;
        this.wasLoanedLastTimeAt = getDateOfToday();

        Date dateStartTime = null;
        try {
            dateStartTime = dateFormat.parse(this.wasLoanedLastTimeAt);
        } catch (ParseException ex) {
           System.out.println(ex);
        }
        Calendar cal = Calendar.getInstance();
        cal.setTime(dateStartTime); 
        cal.add(Calendar.MONTH, 1);
        this.loanExpires = dateFormat.format(cal.getTime());
        this.setAsUnAvailible();
        return loanExpires;
        
        
    }
     private String getDateOfToday() {
        return dateFormat.format(new Date());
    }
     
    public boolean isAvailibe(){
        return isAvailible;
    }
    
    public void setAsUnAvailible(){
    
        this.isAvailible = false;
    }
    
    public String messageLoanExpiresDate(){
    
        return loanExpires;
    }
   
    
}
