package bibliotek;

import java.io.BufferedReader;
import java.io.Console;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Prompt {
    
    private Scanner scanner;
    Console console = null;
    
    public Prompt() {
        
        
        
    }
    
//    public boolean ask(String question, String answer) {
//        System.out.print(question);
//
//        String input = scanner.next();
//        
//        return input.equals(answer);
//    }
    public String ask(String question) {
        //scanner = new Scanner(System.in);
        String input = "";
        
         console = System.console();
        //do{
            try{  
               
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print(question);
                input = br.readLine();
                
                }catch(Exception ex){    
             // if any error occurs
                ex.printStackTrace();      
                }
        //}while(input.equals(""));
        //close();
        return input;
    }
    
    
    public void println(String s){
        
        System.out.println(s);
        
    }
    
    public void close() {
        scanner.close();
    }
}
