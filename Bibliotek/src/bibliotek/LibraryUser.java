/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package bibliotek;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibraryUser {
    
    public String name;
    public String familyName;
    public int userID;
    public String personNumber;
    public String postAdress;
    public String postNumber;
    public String city;
    public String email;
    public ArrayList<LibraryLoan> bookLoans;
    
    public LibraryUser(int userID, String name, String familyName ){
           this.userID = userID;
           this.name = name;
           this.familyName = familyName; 
           bookLoans = new ArrayList<LibraryLoan>();
    }
    public LibraryUser(){}
    
    public void registerALoan(Book b){
        LibraryLoan bookLoan = new LibraryLoan(b.getBookID());
        bookLoans.add(bookLoan);
        System.out.println("Boklånet har registrerats");
    
    }
    public String getName(){
        return name+ " "+familyName;
    }
    public int getID(){
        return userID;
    }
    
    public ArrayList<LibraryLoan> getUserLoans(){
    
        return bookLoans;
    }
    public String getUserData(){
      String data;
        data = String.format("%s userID: %d", getName(),userID);
      return data;
    }
    
    public void unregisterALoan(int bookID){
        
        for(LibraryLoan loan : bookLoans){
            System.out.println("loan.getBookIDFromBookLoan()" + loan.getBookIDFromBookLoan());
            System.out.println("Enterd for-loop");
            if(loan.getBookIDFromBookLoan() == bookID){
                System.out.println("Found book ID " + bookID );
                bookLoans.remove(loan);
                System.out.println("Boklånet har avregistrerats");
                break;
            }
        }
     
    }
    
    
    
    public boolean checkIfBookLoanRegistered(int bookID){
        for(LibraryLoan book : bookLoans){
            if(book.bookID == bookID){
                return true;
            }
        }
        return false;
    
    }
    
}
