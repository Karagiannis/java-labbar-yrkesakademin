/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibrarySession_UserSession extends LibrarySession_{

    ArrayList<LibraryCommand_> customerCommands;
    
    public LibrarySession_UserSession(Prompt prompt,
            ArrayList<LibraryObject_LibraryBook> books,
            ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            LibraryEventsInterface libEvents)
    {
        super(prompt, books, libraryUsers, libraryLoans, libEvents);
        
        customerCommands = new ArrayList<>();
        customerCommands.add(new LibraryCommand_RegisterLoanCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        customerCommands.add(new LibraryCommand_SearchCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        customerCommands.add(new LibraryCommand_LogoutCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
       
        
    
    }

    @Override
    public void start() 
    {
        
        if(login())
        {
        
            String input;
           int inputCode = 0;
           do{
               for (int i = 0; i < customerCommands.size(); i++)
               {
                   prompt.println(i + " - " + customerCommands.get(i).getCommand());

               }
               try{
                    prompt.print("Vad vill du göra? >");
                    inputCode = Integer.parseInt( prompt.readLine());
                    if(inputCode > 2 || inputCode < 0)
                        throw new Exception();
                    else
                        customerCommands.get(inputCode).execute();
                  }catch(Exception e){
                      prompt.println("Måste ange siffrorna 0 till 2");
                      prompt.println(e.toString());
                  }
           }while(inputCode != 2);
        }else{
                   
                prompt.println("Försök igen");
           }
    }
        
     @Override
    public boolean login(){
     
    boolean temp = super.login();
     
    return true;
    }
    
}
