/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;


import gui.UserGUI;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Group;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.canvas.Canvas;
import javafx.stage.Stage;

/**
 *
 * @author Lasse
 */
public class Library{
/**
     * @param args the command line arguments
     */
    // TODO code application logic here
        Prompt prompt = new Prompt();
        ArrayList<LibraryObject_LibraryBook> books = new ArrayList<>();
        ArrayList<LibraryObject_LibraryUser> libraryUsers = new ArrayList<>();
        ArrayList<LibraryObject_LibraryLoan> libraryLoans = new ArrayList<>();
        public LibraryEvents_LibraryLoansToXMLFileHandler events = new LibraryEvents_LibraryLoansToXMLFileHandler(new LibraryEvents_LibraryUsersToXMLFileHandler
            (new LibraryEvents_LibraryBooksToXMLFileHandler(new LibraryEventsCmdLine(books,libraryUsers,libraryLoans, prompt),books),libraryUsers),libraryLoans);
        ArrayList<LibraryCommand_> customerCommands;
        ArrayList<LibraryCommand_> employeeCommands;
        
        
        public Library(){
            customerCommands = new ArrayList<>();
            employeeCommands = new ArrayList<>();
        
        }
    
    
//    public static void main(String[] args) throws UnsupportedEncodingException {
//        
//        
//        System.out.println("Nu startar appen i main");
//        Library library = new Library();
//        library.run();
//        //Application.launch(FXMLExample.class, args);
//        //UserGUI gui = new UserGUI();
//       
//    }
//    
        
   
    public void start(Stage stage) throws Exception {
        
    }
        
        

    public void run(){
    
     
        while(true)
        {
                int command = (events.WelcomeString());
                LibrarySession_ session;

                switch(command)
                {
                    case 1:
                        session = new LibrarySession_UserSession(prompt,books,libraryUsers, libraryLoans, events);
                        session.start();
                        break;
                    case 2:
                        session = new LibrarySession_EmployeeSession(prompt,books,libraryUsers, libraryLoans, events);
                        session.start();
                        break;
                    
                }           
        }   
    }   
public void runTemporarily(){
    
     
        
                int command = (events.WelcomeString());
                LibrarySession_ session;

                switch(command)
                {
                    case 1:
                        session = new LibrarySession_UserSession(prompt,books,libraryUsers, libraryLoans, events);
                        session.start();
                        break;
                    case 2:
                        session = new LibrarySession_EmployeeSession(prompt,books,libraryUsers, libraryLoans, events);
                        session.start();
                        break;
                    
                }           
          
    }   
    
}
