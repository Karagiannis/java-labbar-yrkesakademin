/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibraryCommand_DeRegisterLoanCommand extends LibraryCommand_{
    
    LibraryCommand_DeRegisterLoanCommand(ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            ArrayList<LibraryObject_LibraryBook> libraryBooks,
            Prompt prompt,
            LibraryEventsInterface libEvents){
            super(libraryUsers, libraryLoans,libraryBooks,prompt, libEvents);
    }

    @Override
    protected String getCommand() {
        return "Avregistrera ett lån"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected LibraryObject_ onExecute() {
        
        return libEvents.deRegisterALoan();
//        LibraryObject_LibraryLoan loan = null;
//        String userID = prompt.ask("Låntagarens ID: ");
//        int iUserID = Integer.parseInt(userID);
//        
//        String bookID = prompt.ask("Bokens ID: ");
//        int ibookID  = Integer.parseInt(userID);
//        
//        for(LibraryObject_LibraryUser libraryUser : libraryUsers){
//            if(iUserID == libraryUser.userID){
//                libraryUser.unregisterALoan(ibookID);
//            }       
//        }
//        return loan;
    }
    
}
