/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

/**
 *
 * @author Lasse
 */
    
import dom_package.DomUtil;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerConfigurationException;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import librarysupporxml.LibrarySupport;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

public class LibraryEvents_LibraryLoansToXMLFileHandler extends LibraryEventsDecorator{
    
    ArrayList<LibraryObject_LibraryLoan> libraryLoans;
    String filePathLibraryLoans = "c:\\LOG\\libraryLoans.xml";

    
    public LibraryEvents_LibraryLoansToXMLFileHandler(LibraryEventsInterface libEvents, ArrayList<LibraryObject_LibraryLoan> libraryLoans) {
        
        super(libEvents);
        this.libraryLoans = libraryLoans;
        
        //Transfer data from xml-file to datastructure loans, every time program is started
           loadLoansFromXMLFileToDataStructure();
           
    }

    
           
   
    
    private void loadLoansFromXMLFileToDataStructure(){
    
    
        //Populate datastructure libraryLoans
            try{
                    File fXmlFile = new File(filePathLibraryLoans);
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(fXmlFile);
                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    //NodeList nList = doc.getChildNodes();
                    Element e =  doc.getDocumentElement();
                    //Element first = DomUtil.getFirstElementChild(e);
                    ArrayList<Element> elemArray = DomUtil.getChildElements(e);
                    System.out.println(e.getNodeName() + "  [OPEN]");
                        for (int i = 0; i < elemArray.size(); i++)
                        {
                            System.out.println("Size "+ elemArray.size());
                           
                            LibraryObject_LibraryLoan b = new LibraryObject_LibraryLoan();
                            //if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                           
                            DomUtil.LibraryLoansFillingDataRoutine(elemArray.get(i), 1, b, libraryLoans);
                            libraryLoans.add(b);
                        }
                    System.out.println(e.getNodeName() + "  [CLOSE]");
                }catch (Exception e) 
                {
                     System.out.println("Exception thrown  now :" + e);
                } 
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            for(LibraryObject_LibraryLoan loan : libraryLoans)
               System.out.println( loan.getLoanData());
    
    }
    
     @Override
    public boolean quitProgram() {
        return super.quitProgram(); //To change body of generated methods, choose Tools | Templates.
    }
     @Override
    public LibraryObject_LibraryLoan deRegisterALoan() {
        
        LibraryObject_LibraryLoan loan = super.deRegisterALoan(); //To change body of generated methods, choose Tools | Templates.
        
        try{
                // skapa Documetbuilderfactory
               DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
               // skapa documetbilder
               DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
               // läsa hela XML filen 
               Document docLibraryLoans = dBuilder.parse(new FileInputStream(new File(filePathLibraryLoans)));

               //root 
               //ny elemnts
               NodeList nodeList = docLibraryLoans.getElementsByTagName("loan");

               int find=0;
               for(int i = 0; i < nodeList.getLength(); i++){
                   Node node =  nodeList.item(i);
                   Element e = (Element) node;
                   String id = e.getAttribute("id");
                   if(Integer.toString(loan.loanID).equals(id))
                   nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
                   LibrarySupport.WriteDOMtoXMLFile(docLibraryLoans,filePathLibraryLoans);
               }
                     
            }catch(ParserConfigurationException | SAXException | IOException | DOMException | TransformerException e){
		   System.out.println("Exception :"+e);
	   }
        
        
        return loan;
    }


    @Override
    public LibraryObject_LibraryLoan registerALoan() {
        LibraryObject_LibraryLoan loan =  super.registerALoan(); //To change body of generated methods, choose Tools | Templates.
        
        if(loan != null){
            try{
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();      
                    Document document = documentBuilder.parse(filePathLibraryLoans);
                    Element root = document.getDocumentElement();
                    Element newLoan = document.createElement("loan");
                    Attr id_Attr = document.createAttribute("id");
                    String id = Integer.toString(loan.loanID);
                    id_Attr.setValue(id);
                    newLoan.setAttributeNode(id_Attr);
                    root.appendChild(newLoan);

                     Element tagName = document.createElement("media_id");
                                    tagName.appendChild(document.createTextNode(Integer.toString(loan.bookID)));
                                    newLoan.appendChild(tagName);

                    tagName = document.createElement("user_id");
                                    tagName.appendChild(document.createTextNode(Integer.toString(loan.userID)));
                                    newLoan.appendChild(tagName);

                    tagName = document.createElement("loan_date");
                                    tagName.appendChild(document.createTextNode(loan.loanDate));
                                    newLoan.appendChild(tagName);

                    tagName = document.createElement("expires_date");
                                    tagName.appendChild(document.createTextNode(loan.expiresDate));
                                    newLoan.appendChild(tagName);

                    
                    LibrarySupport.WriteDOMtoXMLFile(document,filePathLibraryLoans);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
        return loan;
    }
    
   
    
    
    
    
    
    /*********************** MUST HAVE METHODS *********************************/

    @Override
    public LibraryObject_LibraryUser registerAUser() {
        return super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
    }
    @Override
    public LibraryObject_LibraryUser deRegisterAUser() {
        return super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean extractUserDataFromID(int id) {
        return super.extractUserDataFromID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findUserIDFromName(String fullName) {
        return super.findUserIDFromName(fullName); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchUserData() {
        super.searchUserData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean extractBookDataFrombookID(int id) {
        return super.extractBookDataFrombookID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findBookIDFromTitle(String title) {
        return super.findBookIDFromTitle(title); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchTitleData() {
        super.searchTitleData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchAuthorData() {
        super.searchAuthorData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void search() {
        super.search(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LibraryObject_LibraryBook deRegisterABook() {
        return super.deRegisterABook(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LibraryObject_LibraryBook registerABook() {
        return super.registerABook(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int WelcomeString() {
        return super.WelcomeString(); //To change body of generated methods, choose Tools | Templates.
    }
     @Override
    public int findLoginData(String fullName, String PassWord){
        return super.findLoginData(fullName, PassWord);
    }
    
}
