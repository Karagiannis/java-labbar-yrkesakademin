/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibrarySession_EmployeeSession extends LibrarySession_{

    ArrayList<LibraryCommand_> employeeCommands;
    
        
   
    public LibrarySession_EmployeeSession(Prompt prompt,
            ArrayList<LibraryObject_LibraryBook> books,
            ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            LibraryEventsInterface libEvents)
            
     {
        super( prompt,books,libraryUsers, libraryLoans, libEvents);
        employeeCommands = new ArrayList<>();
        employeeCommands.add(new LibraryCommand_RegisterBookCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        employeeCommands.add(new LibraryCommand_RegisterUserCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        employeeCommands.add(new LibraryCommand_RegisterLoanCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        employeeCommands.add(new LibraryCommand_DeRegisterLoanCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        employeeCommands.add(new LibraryCommand_SearchCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        employeeCommands.add(new LibraryCommand_LogoutCommand(libraryUsers,libraryLoans,books,prompt,libEvents));
        

    }

    

    /**
     *
     */
    @Override
    public void start() 
    {
        LibraryObject_ obj;
        String input;
        int inputCode = 0;
        do{
            for (int i = 0; i < employeeCommands.size(); i++){
                prompt.println(i + " - " + employeeCommands.get(i).getCommand());

            }
            try{
                prompt.print("Vad vill du göra? >");
                 inputCode = Integer.parseInt( prompt.readLine());
                 if(inputCode > 5 || inputCode < 0)
                     throw new Exception();
                 else
                    obj =  employeeCommands.get(inputCode).execute();
               }catch(Exception e){
                   prompt.println("Måste ange siffrorna 1 till 6");
                   prompt.println(e.toString());
               }
            
            
        }while(inputCode != 5);
    }
    
    @Override
    public boolean login(){
     
    boolean temp = super.login();
    
    return true;
     
    }
    
    
}
