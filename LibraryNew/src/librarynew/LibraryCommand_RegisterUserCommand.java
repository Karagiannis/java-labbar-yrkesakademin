/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibraryCommand_RegisterUserCommand extends LibraryCommand_{
    
    
    public LibraryCommand_RegisterUserCommand(ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            ArrayList<LibraryObject_LibraryBook> libraryBooks,
            Prompt prompt,
            LibraryEventsInterface libEvents){
        
            super(libraryUsers, libraryLoans,libraryBooks,prompt, libEvents);
    
    
    }

    @Override
    protected String getCommand() {
        return "Registrera en användare";
    }

    @Override
    protected LibraryObject_ onExecute() {
        
        return libEvents.registerAUser();
//        String name = prompt.ask("Ny låntagares, förnamn: >");
//        String familyName = prompt.ask("Ny låntagares, efternamn: >");
//        String personNumber = prompt.ask("Ny låntagares, personnummer (yy-mm-dd-xxxx) >");
//        String postAdress = prompt.ask("Ny låntagares, post-adress: >");
//        String postNumber = prompt.ask("Ny låntagares, postnummer : >");
//        String city = prompt.ask("Ny låntagares, ort: >");
//        String email = prompt.ask("Ny låntagares, e-mail adress: >");
//        
//        int numberIDOfNewUser = libraryUsers.size();
//        LibraryObject_LibraryUser user = new LibraryObject_LibraryUser(numberIDOfNewUser, name, familyName);
//        user.personNumber = personNumber;
//        user.postAdress = postAdress;
//        user.postNumber = postNumber;
//        user.city = city;
//        user.email = email;
//        
//        libraryUsers.add(user);
//        
//        System.out.println("Du registrerade användar ID: " + numberIDOfNewUser );
//        System.out.println("förnamn: " + name);
//        System.out.println("efternamn: " + familyName);
//        return user;//To change body of generated methods, choose Tools | Templates.

           
    }
    
}
