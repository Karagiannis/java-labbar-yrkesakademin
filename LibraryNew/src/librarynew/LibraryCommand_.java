/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Lasse
 */
 public abstract class LibraryCommand_ {
     
        protected final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        ArrayList<LibraryObject_LibraryUser> libraryUsers = null;
        ArrayList<LibraryObject_LibraryLoan> libraryLoans = null;
        ArrayList<LibraryObject_LibraryBook> libraryBooks = null;
        Prompt prompt = null;
        LibraryEventsInterface libEvents;
                
     
     
     public LibraryCommand_(ArrayList<LibraryObject_LibraryUser> libraryUsers,
                        ArrayList<LibraryObject_LibraryLoan> libraryLoans,
                        ArrayList<LibraryObject_LibraryBook> libraryBooks,
                        Prompt prompt,
                       LibraryEventsInterface libEvents){
                this.libraryUsers = libraryUsers;
                this.libraryLoans = libraryLoans;
                this.libraryBooks = libraryBooks;
                this.prompt = prompt;
                this.libEvents = libEvents;
     }
     
     public LibraryCommand_(ArrayList<LibraryObject_LibraryLoan> libraryLoans,
                                ArrayList<LibraryObject_LibraryBook> libraryBooks, Prompt prompt,
                                LibraryEventsInterface libEvents){
         this.prompt = prompt;
         this.libraryBooks = libraryBooks;
         this.libraryLoans = libraryLoans;
         this.libEvents = libEvents;
     }
    
     public LibraryObject_ execute(){
     
         return onExecute();
     }
    
     protected abstract String getCommand();
    
     protected abstract LibraryObject_ onExecute();  
     
     protected String getTodaysDate() {
        return dateFormat.format(new Date());//To change body of generated methods, choose Tools | Templates.
    }

    protected String getExpirationDate() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 30); //minus number would decrement the days
        date = cal.getTime();
        return dateFormat.format(date); //To change body of generated methods, choose Tools | Templates.
    }

}
