/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibraryCommand_RegisterBookCommand extends LibraryCommand_{
    
    public LibraryCommand_RegisterBookCommand(ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            ArrayList<LibraryObject_LibraryBook> libraryBooks,
            Prompt prompt,
            LibraryEventsInterface libEvents){
            super(libraryUsers, libraryLoans,libraryBooks,prompt, libEvents);
    
    }

    @Override
    protected String getCommand() {
        return "Registrera en bok"; //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    protected LibraryObject_ onExecute() {
        
        return libEvents.registerABook();
//       LibraryObject_LibraryBook book = null;   
//        
//     String title = prompt.ask("Bokens titel: ");
//     String author = prompt.ask("Bokens författare (Efternamn, Namn): ");
//     String availibleCategories =   "Välj kategori\n" +
//                                    "1 - Teknik\n"+
//                                    "2 - Skönlitteratur\n" +
//                                    "3 - Tdining \n" +
//                                    "4 - CD/DVD\n" ;
//     
//     
//     String categoryIdString = prompt.ask(availibleCategories);
//     String category = "";
//     int categoryId;
//     try{
//        
//        categoryId = Integer.parseInt(categoryIdString);
//        switch (categoryId){
//
//            case 1:
//                category = "Teknik";
//                break;
//            case 2:
//                category = "Skönlitteratur";
//                break;
//            case 3:
//                category = "Tidning";
//                break;
//            case 4:
//                category = "CD/DVD";
//                break;
//
//        }
//     }catch(Exception e){
//              System.out.println("Måste ange siffrorna 1 till 4");
//              System.out.println(e);
//              return book;    
//          }
//     
//     String price = prompt.ask("Pris");
//     String publishDate = prompt.ask("Publiceringsdatum ÅÅÅÅ-MM-DD: ");
//     String description = prompt.ask("Beskrivning: ");
//     Boolean isAvailible = true;
//     String wasLoanedLastTime = " 1970-01-01";
//     String loanExpires = " 1970-01-01";
//     String today =  getTodaysDate();
//     book = new LibraryObject_LibraryBook( libraryBooks.size(), title, author, category );
//     book.price = price;
//     book.publish_date = publishDate;
//     book.description = description;
//     book.isAvailible = true;
//     book.wasLoanedLastTimeAt = wasLoanedLastTime;
//     book.loanExpires = loanExpires;
//     book.registeredAtDate = today;
//     
//     
//     
//     libraryBooks.add(book);
//     System.out.println("You registered book ID: " + libraryBooks.size() );
//     System.out.println("title: " + title);
//     System.out.println("author: " + author);
//     System.out.println("category: " + category );
//     
//      return book;    //To change body of generated methods, choose Tools | Templates.
    }
    
}
