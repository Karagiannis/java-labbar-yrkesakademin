/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import dom_package.DomUtil;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.TransformerException;
import librarysupporxml.LibrarySupport;
import org.w3c.dom.Attr;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

/**
 *
 * @author Lasse
 */
public class LibraryEvents_LibraryUsersToXMLFileHandler extends LibraryEventsDecorator{
    
   
    ArrayList<LibraryObject_LibraryUser> libraryUsers;
    
    String filePathUsers = "c:\\LOG\\libraryUsers.xml";
   
    
    public LibraryEvents_LibraryUsersToXMLFileHandler(LibraryEventsInterface libEvents, ArrayList<LibraryObject_LibraryUser> libraryUsers) {
        super(libEvents);
        this.libraryUsers = libraryUsers;
         //Transfer data from xml-file to datastructure users, every time program is started
           loadUsersFromXMLFileToDataStructure();
    }

    @Override
    public LibraryObject_LibraryUser registerAUser() {
        LibraryObject_LibraryUser user =  super.registerAUser(); //To change body of generated methods, choose Tools | Templates.
        
        if(user != null){
            try{
                    DocumentBuilderFactory documentBuilderFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder documentBuilder = documentBuilderFactory.newDocumentBuilder();      
                    Document document = documentBuilder.parse(filePathUsers);
                    Element root = document.getDocumentElement();
                    Element newUser = document.createElement("user");
                    Attr id_Attr = document.createAttribute("id");
                    String id = Integer.toString(user.getID());
                    id_Attr.setValue(id);
                    newUser.setAttributeNode(id_Attr);
                    root.appendChild(newUser);

                     Element tagName = document.createElement("forenamn");
                                    tagName.appendChild(document.createTextNode(user.name));
                                    newUser.appendChild(tagName);

                    tagName = document.createElement("efternamn");
                                    tagName.appendChild(document.createTextNode(user.familyName));
                                    newUser.appendChild(tagName);

                    tagName = document.createElement("personnummer");
                                    tagName.appendChild(document.createTextNode(user.personNumber));
                                    newUser.appendChild(tagName);

                    tagName = document.createElement("adress");
                                    tagName.appendChild(document.createTextNode(user.postAdress));
                                    newUser.appendChild(tagName);
                                    
                    tagName = document.createElement("ort");
                                    tagName.appendChild(document.createTextNode(user.city));
                                    newUser.appendChild(tagName);
                                    
                    tagName = document.createElement("email");
                                    tagName.appendChild(document.createTextNode(user.email));
                                    newUser.appendChild(tagName);

                    
                    LibrarySupport.WriteDOMtoXMLFile(document,filePathUsers);
            }catch(Exception e){
                e.printStackTrace();
            }
        }
        
        return user;
        
       
    }
    
    
    @Override
    public LibraryObject_LibraryUser deRegisterAUser() {
        LibraryObject_LibraryUser user = super.registerAUser(); //To change body of generated methods, choose Tools | Templates.   
        
        try{
                // skapa Documetbuilderfactory
               DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
               // skapa documetbilder
               DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
               // läsa hela XML filen 
               Document document = dBuilder.parse(new FileInputStream(new File(filePathUsers)));

               //root 
               //ny elemnts
               NodeList nodeList = document.getElementsByTagName("user");

               int find=0;
               for(int i = 0; i < nodeList.getLength(); i++){
                   Node node =  nodeList.item(i);
                   Element e = (Element) node;
                   String id = e.getAttribute("id");
                   if(Integer.toString(user.getID()).equals(id))
                   nodeList.item(i).getParentNode().removeChild(nodeList.item(i));
                   LibrarySupport.WriteDOMtoXMLFile(document,filePathUsers);
               }
                     
            }catch(ParserConfigurationException | SAXException | IOException | DOMException | TransformerException e){
		   System.out.println("Exception :"+e);
	   }
        
        return user;
    }
        
    private void loadUsersFromXMLFileToDataStructure(){
        
        //Populate datastructure libraryUsers
            try{
                    //File fXmlFile = new File(filePathUsers);
                    FileInputStream in = new FileInputStream(new File(filePathUsers));
                    DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
                    DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
                    Document doc = dBuilder.parse(in, "UTF-8");
                    doc.getDocumentElement().normalize();

                    System.out.println("Root element :" + doc.getDocumentElement().getNodeName());
                    //NodeList nList = doc.getChildNodes();
                    Element e =  doc.getDocumentElement();
                    //Element first = DomUtil.getFirstElementChild(e);
                    ArrayList<Element> elemArray = DomUtil.getChildElements(e);
                    System.out.println(e.getNodeName() + "  [OPEN]");
                        for (int i = 0; i < elemArray.size(); i++)
                        {
                            System.out.println("Size "+ elemArray.size());
                           
                            LibraryObject_LibraryUser b = new LibraryObject_LibraryUser();
                            //if (nList.item(i).getNodeType() == Node.ELEMENT_NODE)
                           
                            DomUtil.LibraryUsersFillingDataRoutine(elemArray.get(i), 1, b, libraryUsers);
                            libraryUsers.add(b);
                        }
                    System.out.println(e.getNodeName() + "  [CLOSE]");
                }catch (Exception e) 
                {
                     System.out.println("Exception thrown  now :" + e);
                } 
            System.out.println("%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%");
            for(LibraryObject_LibraryUser user : libraryUsers)
               System.out.println( user.getUserData());
        
    }

    @Override
    public boolean quitProgram() {
        return super.quitProgram(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean extractUserDataFromID(int id) {
        return super.extractUserDataFromID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findUserIDFromName(String fullName) {
        return super.findUserIDFromName(fullName); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchUserData() {
        super.searchUserData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public boolean extractBookDataFrombookID(int id) {
        return super.extractBookDataFrombookID(id); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int findBookIDFromTitle(String title) {
        return super.findBookIDFromTitle(title); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchTitleData() {
        super.searchTitleData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void searchAuthorData() {
        super.searchAuthorData(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public void search() {
        super.search(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LibraryObject_LibraryBook deRegisterABook() {
        return super.deRegisterABook(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LibraryObject_LibraryBook registerABook() {
        return super.registerABook(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LibraryObject_LibraryLoan registerALoan() {
        return super.registerALoan(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public LibraryObject_LibraryLoan deRegisterALoan() {
        return super.deRegisterALoan(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public int WelcomeString() {
        return super.WelcomeString(); //To change body of generated methods, choose Tools | Templates.
    }

     @Override
    public int findLoginData(String fullName, String PassWord){
        return super.findLoginData(fullName, PassWord);
    }
    
}
