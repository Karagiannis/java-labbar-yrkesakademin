/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.text.SimpleDateFormat;
import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public abstract class LibraryEventsDecorator implements LibraryEventsInterface {
    
    LibraryEventsInterface libEvents;
    
    
    public ArrayList<LibraryObject_LibraryBook> books;
    public ArrayList<LibraryObject_LibraryUser> libraryUsers;
    protected final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    protected  Prompt prompt;

    public LibraryEventsDecorator( LibraryEventsInterface libEvents) {
        this.libEvents = libEvents;
    }

    @Override
    public int WelcomeString(){
        return libEvents.WelcomeString();
    }
    /****************************************************************/
    /************************* Registration event
     * @return s******************/
    @Override
    public  LibraryObject_LibraryLoan deRegisterALoan(){
        return libEvents.deRegisterALoan();
    }
    /******************** Registation        bookloan *************************/
    @Override
    public LibraryObject_LibraryLoan registerALoan(){
        return libEvents.registerALoan();
    }
    /*******************Registration       book data**************************/
    @Override
    public LibraryObject_LibraryBook registerABook(){
    
        return libEvents.registerABook();
    }

    @Override
    public  LibraryObject_LibraryBook deRegisterABook(){
        return libEvents.deRegisterABook();
    }
    
    

    /****************************  Registration  user data******************************/
    @Override
    public LibraryObject_LibraryUser registerAUser(){
        return libEvents.registerAUser();
    }

    @Override
    public LibraryObject_LibraryUser registerAUserFromGUI(String name, String familyName, String personNumber,
            String postAdress, String postNumber, String city, String email){
        return libEvents.registerAUser();
    }
    @Override
    public abstract LibraryObject_LibraryUser deRegisterAUser();

   

    /**********************************************************************/
    /***********************Searching**************************************/
   @Override
    public  void search(){
        libEvents.search();
    }

    /********Utility method with pattern matchin not currently used**********/
    
    

    /*************Searching      Author data related************************/
    @Override
    public  void searchAuthorData(){
        libEvents.searchAuthorData();
    }

    /****************Searching        LibraryObject_LibraryBook related**************************/
    @Override
    public void searchTitleData(){
        libEvents.searchTitleData();
    }

    @Override
    public  int findBookIDFromTitle(String title){
    
        return libEvents.findBookIDFromTitle(title);
    }

    @Override
    public boolean extractBookDataFrombookID(int id){
    
        return libEvents.extractBookDataFrombookID(id);
    }

    /**************Searching       User data related**************************/
    @Override
    public void searchUserData(){
        libEvents.searchUserData();
    
    }

    @Override
    public  int findUserIDFromName(String fullName){
        return libEvents.findUserIDFromName(fullName);
    }

    @Override
    public  boolean extractUserDataFromID(int id){
        return libEvents.extractUserDataFromID(id);
    }
    @Override
    public  boolean quitProgram(){
        return libEvents.quitProgram();
    }
    @Override
    public int findLoginData(String fullName, String PassWord){
        return libEvents.findLoginData(fullName, PassWord);
    }
    
}
