/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 *
 * @author Lasse
 */
public class LibraryEventsCmdLine implements LibraryEventsInterface {
    
        ArrayList<LibraryObject_LibraryBook> books;
        ArrayList<LibraryObject_LibraryUser> libraryUsers;
        ArrayList<LibraryObject_LibraryLoan> libraryLoans;
        Prompt prompt;
        private final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
    
    
    public LibraryEventsCmdLine(ArrayList<LibraryObject_LibraryBook> books, ArrayList<LibraryObject_LibraryUser> libraryUsers,
                                     ArrayList<LibraryObject_LibraryLoan> libraryLoans, Prompt prompt){
        
        
        this.books = books;
        this.libraryUsers = libraryUsers;
        this.libraryLoans = libraryLoans;
        this.prompt = prompt;
    }
    
    @Override
    public int WelcomeString()
    {
      String questionLogin = "1 - Användar nam\n" +
                             "2 - Anställd  >";
      
      String questionLoginUserName = " - Användar nam  >";
      String questionLoginPassword = " - Lösenord  >";
      
      
      int loginCode = 0;
      int categoryID = 0;
      String username ="";
      String password = "";
      username = prompt.ask(questionLoginUserName);
      password = prompt.ask(questionLoginPassword);
      return 2;
      
//       try{
//            username = prompt.ask(questionLoginUserName);
//            
//            
//          }catch(Exception e){
//              prompt.println("Username problem");
//              prompt.println(e.toString());
//              //return loginCode;
//              
//          }
//          try{
//                password = prompt.ask(questionLoginPassword);
//                categoryID = findLoginData(username, password);
//                return categoryID;
//           }catch(Exception e){
//              //prompt.println("Måste ange siffrorna 1 eller 2");
//              prompt.println(e.toString());
//              //return loginCode;
//              
//          }
//       return categoryID;
      
//      try{
//            loginCode = Integer.parseInt(prompt.ask(questionLogin));
//          }catch(Exception e){
//              prompt.println("Måste ange siffrorna 1 eller 2");
//              prompt.println(e.toString());
//              return loginCode;
//              
//          }
//      if( loginCode == 1)
//          return new UserSession();
//      else
//          return new EmployeeSession();
    //return loginCode;
    }
      
   
   /****************************************************************/
    /************************* Registration events******************/
    
    /**************REgister a book retur
     * @return n**************************/
    @Override
    public LibraryObject_LibraryLoan deRegisterALoan(){
        
        LibraryObject_LibraryLoan loan = null;
        String userID = prompt.ask("Låntagarens ID: ");
        int iUserID = Integer.parseInt(userID);
        
        String bookID = prompt.ask("Bokens ID: ");
        int ibookID  = Integer.parseInt(userID);
        
        for(LibraryObject_LibraryUser libraryUser : libraryUsers){
            if(iUserID == libraryUser.userID){
                libraryUser.unregisterALoan(ibookID);
            }       
        }
        return loan;
    }
    /******************** Registation        bookloan *************************/
    @Override
    public LibraryObject_LibraryLoan registerALoan(){
        LibraryObject_LibraryLoan loan = null;
        String userID = prompt.ask("Låntagarens ID: ");
        int iUserID = Integer.parseInt(userID);
        System.out.println(String.format("%d",iUserID ));
        
        String bookID = prompt.ask("Bokens ID: ");
        int ibookID  = Integer.parseInt(bookID);
        System.out.println(String.format("Boken som söks har ID %d",ibookID ));
        
        for(LibraryObject_LibraryUser libraryUser : libraryUsers){
            if(iUserID == libraryUser.userID)
            {
                System.out.println("Användaren finns i databasen");
                for(LibraryObject_LibraryBook book : books){
                    System.out.println(String.format("Bokens som söks är %d, hittar %d",ibookID, book.getBookID() ));
                    if (book.getBookID() == ibookID)
                    {   System.out.println("Boken existerar");
                        if(book.isAvailibe()){
                            System.out.println("Boken finns tillgänglig");
                            //libraryUser.registerALoan(book);
                            //String loanExpires = book.loan();
                            
                            book.setAsUnAvailible();
                            System.out.println("Lånet har registrerats");
                            //System.out.println(loanExpires);
                            loan = new LibraryObject_LibraryLoan();
                            loan.bookID = book.getBookID();
                            loan.loanID = (int) (libraryLoans.size() + Math.floor( Math.random() * 11111111.1 ));
                            loan.loanDate = getTodaysDate();
                            book.wasLoanedLastTimeAt = loan.loanDate;
                            loan.expiresDate = getExpirationDate();
                            book.loanExpires = loan.expiresDate;
                            loan.userID = iUserID;
                            libraryLoans.add(loan);
                            return loan;
                        }
                        else{
                            System.out.println("Boken är inte tillgänglig");
                            break;
                        }
                    }else{System.out.println("Söker boken/mediat");}
                }
             break;
            } else System.out.println("Söker användaren...");
        }//end for
        return loan;
    }
    
    
    
    /*******************Registration       book data**************************/
    @Override
    public LibraryObject_LibraryBook registerABook(){
        
    LibraryObject_LibraryBook book = null;   
        
     String title = prompt.ask("Bokens titel: ");
     String author = prompt.ask("Bokens författare (Efternamn, Namn): ");
     String availibleCategories =   "Välj kategori\n" +
                                    "1 - Teknik\n"+
                                    "2 - Skönlitteratur\n" +
                                    "3 - Tdining \n" +
                                    "4 - CD/DVD\n" ;
     
     
     String categoryIdString = prompt.ask(availibleCategories);
     String category = "";
     int categoryId;
     try{
        
        categoryId = Integer.parseInt(categoryIdString);
        switch (categoryId){

            case 1:
                category = "Teknik";
                break;
            case 2:
                category = "Skönlitteratur";
                break;
            case 3:
                category = "Tidning";
                break;
            case 4:
                category = "CD/DVD";
                break;

        }
     }catch(Exception e){
              System.out.println("Måste ange siffrorna 1 till 4");
              System.out.println(e);
              return book;    
          }
     
     String price = prompt.ask("Pris");
     String publishDate = prompt.ask("Publiceringsdatum ÅÅÅÅ-MM-DD: ");
     String description = prompt.ask("Beskrivning: ");
     Boolean isAvailible = true;
     String wasLoanedLastTime = " 1970-01-01";
     String loanExpires = " 1970-01-01";
     String today =  getTodaysDate();
     book = new LibraryObject_LibraryBook( books.size(), title, author, category );
     book.price = price;
     book.publish_date = publishDate;
     book.description = description;
     book.isAvailible = true;
     book.wasLoanedLastTimeAt = wasLoanedLastTime;
     book.loanExpires = loanExpires;
     book.registeredAtDate = today;
     
     
     
     books.add(book);
     System.out.println("You registered book ID: " + books.size() );
     System.out.println("title: " + title);
     System.out.println("author: " + author);
     System.out.println("category: " + category );
     
      return book;    
    }
    
    public LibraryObject_LibraryBook deRegisterABook(){
    
        LibraryObject_LibraryBook b = null;
        String bookIDString = prompt.ask("Ange bok ID som skall avregistreras >");
        int bookID = Integer.parseInt(bookIDString);
        for (LibraryObject_LibraryBook book : books){
           if (book.getBook(bookID) != null){
               b = book.getBook(bookID);
               break;
           }
        }
        if(b != null)
        {
            System.out.println(b.getBookData());
            String yesNoAnswer = prompt.ask("Är det denna som ska  avregistreras? (y/n) >");
            if(yesNoAnswer.equals("y") ||yesNoAnswer.equals("Y") ){
               books.remove(b); 
               System.out.println("Boken avregistrerades");
               return b;
            }
            else b = null;
        }else{
            System.out.println("Boken finns ej registrerad");
        }
        return b;
    
    }
     /****************************  Registration  user dat
     * @return a******************************/
    @Override
    public LibraryObject_LibraryUser registerAUser(){
        String category = prompt.ask("Ny låntagare kategori (customer/admin) >");
        String name = prompt.ask("Ny låntagares, förnamn: >");
        String familyName = prompt.ask("Ny låntagares, efternamn: >");
        String personNumber = prompt.ask("Ny låntagares, personnummer (yy-mm-dd-xxxx) >");
        String postAdress = prompt.ask("Ny låntagares, post-adress: >");
        String postNumber = prompt.ask("Ny låntagares, postnummer : >");
        String city = prompt.ask("Ny låntagares, ort: >");
        String email = prompt.ask("Ny låntagares, e-mail adress: >");
        String passWord = prompt.ask("Ny låntagares, e-mail adress: >");
        
        int numberIDOfNewUser = libraryUsers.size();
        LibraryObject_LibraryUser user = new LibraryObject_LibraryUser(numberIDOfNewUser, name, familyName);
        user.personNumber = personNumber;
        user.postAdress = postAdress;
        user.postNumber = postNumber;
        user.city = city;
        user.email = email;
        user.passWord = passWord;
        user.category = category;
        
        libraryUsers.add(user);
        
        System.out.println("Du registrerade användar ID: " + numberIDOfNewUser );
        System.out.println("förnamn: " + name);
        System.out.println("efternamn: " + familyName);
        return user;
        
    }
    @Override
    public LibraryObject_LibraryUser registerAUserFromGUI(String name, String familyName, String personNumber,
            String postAdress, String postNumber, String city, String email){
        
//        String name = prompt.ask("Ny låntagares, förnamn: >");
//        String familyName = prompt.ask("Ny låntagares, efternamn: >");
//        String personNumber = prompt.ask("Ny låntagares, personnummer (yy-mm-dd-xxxx) >");
//        String postAdress = prompt.ask("Ny låntagares, post-adress: >");
//        String postNumber = prompt.ask("Ny låntagares, postnummer : >");
//        String city = prompt.ask("Ny låntagares, ort: >");
//        String email = prompt.ask("Ny låntagares, e-mail adress: >");
        
        int numberIDOfNewUser = libraryUsers.size();
        LibraryObject_LibraryUser user = new LibraryObject_LibraryUser(numberIDOfNewUser, name, familyName);
        user.personNumber = personNumber;
        user.postAdress = postAdress;
        user.postNumber = postNumber;
        user.city = city;
        user.email = email;
        
        libraryUsers.add(user);
        
        System.out.println("Du registrerade användar ID: " + numberIDOfNewUser );
        System.out.println("förnamn: " + name);
        System.out.println("efternamn: " + familyName);
        return user;
        
    }
    
    public LibraryObject_LibraryUser deRegisterAUser(){
       LibraryObject_LibraryUser user = null;
       
        String userIDString = prompt.ask("Ange änvändaren som skall avregistreras >");
        int userID = Integer.parseInt(userIDString);
        for (LibraryObject_LibraryUser u : libraryUsers){
           if (u.getUser(userID) != null){
               user = u.getUser(userID);
               break;
           }
        }
        if(user != null)
        {
            System.out.println(user.getUserData());
            String yesNoAnswer = prompt.ask("Är det denna som ska  avregistreras? (y/n) >");
            if(yesNoAnswer.equals("y") ||yesNoAnswer.equals("Y") ){
               libraryUsers.remove(user); 
               System.out.println("Användaren avregistrerades");
               return user;
            }
            else user = null;
        }else{
            System.out.println("Användaren finns ej registrerad");
        }   
       return user;
    
    }
     private String getTodaysDate() {
        return dateFormat.format(new Date());
    }
    private String getExpirationDate() {
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, 30); //minus number would decrement the days
        date = cal.getTime();
        return dateFormat.format(date);
    }
    
     /**********************************************************************/
     /***********************Searching**************************************/
    @Override
    public void search()
    {
        String searchCategories =   "1 - Titel (bok, cd/dvd, tidning)\n"+
                                    "2 - Författare\n" +
                                    "3 - Låntagar info \n" +
                                    "4 - Avsluta sökning\n" +
                                    "Vad vill du söka? >";
        int categoryId;
      do{
            String categoryIdString = prompt.ask(searchCategories);
            categoryId = Integer.parseInt(categoryIdString);
            switch (categoryId)
            {
                case 1:
                    searchTitleData();
                    break;
                case 2:
                    searchAuthorData();
                    break;
                case 3:
                    searchUserData();
                    break;
                case 4:
                    break; 
            }       
        }while(categoryId != 4);
    }
    
    /********Utility method with pattern matchin not currently used**********/
    private boolean matchString(String input, String expression){
         String REGEX = expression;
         String INPUT = input;
                                    
   
       Pattern p = Pattern.compile(REGEX);
       Matcher m = p.matcher(INPUT); // get a matcher object
     
       if(m.find()) {
         System.out.println("Found match");
         return true;
       }
       else return false;
    }
    
    
    /*************Searching      Author data related************************/
    public void searchAuthorData(){
        String author = prompt.ask("Skriv författarens namn >");
        boolean found = false;
        for(LibraryObject_LibraryBook book : books){
           if (book.getAuthor().equals(author)) {
               found = true;
               System.out.println(book.getBookData());
            }
        }
        if(!found)
            System.out.println("Author was not found");
    }
    
    /****************Searching        LibraryObject_LibraryBook related**************************/
    
        @Override
    public void searchTitleData(){
        String title = prompt.ask("Skriv titeln >");
        extractBookDataFrombookID(findBookIDFromTitle(title));
    }
    
        @Override
    public int findBookIDFromTitle(String title){
        int bookID = -1;
        boolean found = false;
        for(LibraryObject_LibraryBook book : books){
           if (book.getTitle().equals(title)) {
               bookID = book.getBookID();
               found = true;
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return bookID; 
    }
    
        @Override
    public boolean extractBookDataFrombookID(int id){
        boolean found = false;
        if(id != -1)
        {
            for(LibraryObject_LibraryBook book : books){
               if (book.getBookID() == id) {
                   found = true;
                   System.out.println("Bokens registrerade uppgifter:\n" + book.getBookData());
                }
            }
        }
        if(!found)
            System.out.println("Boken finns inte");
        
        return found;
    }
         
    
  /**************Searching       User data related**************************/  
    
        @Override
    public void searchUserData(){
        String name = prompt.ask("Skriv låntagarens namn >");
        extractUserDataFromID(findUserIDFromName(name)); 
    }
    
        @Override
    public int findUserIDFromName(String fullName){
        int userID = -1;
        boolean found = false;
        for(LibraryObject_LibraryUser user : libraryUsers){
           if (user.getName().equals(fullName)) {
               userID = user.getID();
               found = true;
               extractUserDataFromID(userID);
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return userID; 
    }
        @Override
    public int findLoginData(String fullName, String PassWord){
        int userCategoryID = -1;
        boolean found = false;
        for(LibraryObject_LibraryUser user : libraryUsers){
           if (user.getName().equals(fullName)) {
               System.out.println(user.getName());
               if(user.getUserPassWord().equals(PassWord)){
                   System.out.println(user.getUserPassWord());
                   found = true;
                   if(user.category.equals("customer"))
                       return 1;
                   else
                       return 2;
               }
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return userCategoryID; 
        
    }
//    public LibrarySession_ findLoginData(String fullName, String PassWord, Object object){
//        int userCategoryID = -1;
//        boolean found = false;
//        for(LibraryObject_LibraryUser user : libraryUsers){
//           if (user.getName().equals(fullName)) {
//               System.out.println(user.getName());
//               if(user.getUserPassWord().equals(PassWord)){
//                   System.out.println(user.getUserPassWord());
//                   found = true;
//                   if(user.category.equals("customer"))
//                       return new LibrarySession_UserSession(prompt,books,libraryUsers,libraryLoans,null);
//                   else
//                       return new LibrarySession_EmployeeSession(prompt,books,libraryUsers,libraryLoans,null);
//               }
//            }
//        }
//    }
    
        @Override
    public boolean extractUserDataFromID(int id){
        boolean found = false;
        for(LibraryObject_LibraryUser user : libraryUsers){
           if (user.getID() == id) {
               found = true;
               
               System.out.println("Användarens registrerade uppgifter:\n" + user.getUserData());
               
               
               ArrayList<LibraryObject_LibraryLoan> loans = user.getUserLoans();
               if(loans.size() > 0 )
                   System.out.println("Användaren har tidigare registrerade lån\n");
               for (LibraryObject_LibraryLoan loan : loans)
                    for(LibraryObject_LibraryBook book : books)
                        if(book.getBookID() == loan.bookID)
                            System.out.println(book.getBookData());
            }
        }
        if(!found)
            System.out.println("User was not found");
        
        return found;
    }
    
        @Override
    public boolean quitProgram(){
        return true;
    }
}
