/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibraryObject_LibraryUser extends LibraryObject_{
    
    public String name;
    public String familyName;
    public int userID;
    public String personNumber;
    public String postAdress;
    public String postNumber;
    public String city;
    public String email;
    public String passWord;
    public String category;
    public ArrayList<LibraryObject_LibraryLoan> bookLoans;
    
    public LibraryObject_LibraryUser(int userID, String name, String familyName ){
           this.userID = userID;
           this.name = name;
           this.familyName = familyName; 
           bookLoans = new ArrayList<LibraryObject_LibraryLoan>();
    }
    public LibraryObject_LibraryUser(){}
    
    public LibraryObject_LibraryUser getUser(int userID){
        
        if(userID == this.userID)
            return this;
        else
            return null;
    
    }
    public String getUserPassWord(){
        
            return passWord;
    }
    
    
    public void registerALoan(LibraryObject_LibraryBook b){
        LibraryObject_LibraryLoan bookLoan = new LibraryObject_LibraryLoan(b.getBookID());
        bookLoans.add(bookLoan);
        System.out.println("Boklånet har registrerats");
    
    }
    public String getName(){
        return name+ " "+familyName;
    }
    public int getID(){
        return userID;
    }
    
    public ArrayList<LibraryObject_LibraryLoan> getUserLoans(){
    
        return bookLoans;
    }
    public String getUserData(){
      String data;
        data = String.format("%s userID: %d", getName(),userID);
      return data;
    }
    
    public void unregisterALoan(int bookID){
        
        for(LibraryObject_LibraryLoan loan : bookLoans){
            System.out.println("loan.getBookIDFromBookLoan()" + loan.getBookIDFromBookLoan());
            System.out.println("Enterd for-loop");
            if(loan.getBookIDFromBookLoan() == bookID){
                System.out.println("Found book ID " + bookID );
                bookLoans.remove(loan);
                System.out.println("Boklånet har avregistrerats");
                break;
            }
        }
     
    }
    
    
    
    public boolean checkIfBookLoanRegistered(int bookID){
        for(LibraryObject_LibraryLoan book : bookLoans){
            if(book.bookID == bookID){
                return true;
            }
        }
        return false;
    
    }
    
}
