/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public abstract class LibrarySession_ {


    Prompt prompt;
    ArrayList<LibraryObject_LibraryBook> books;
    ArrayList<LibraryObject_LibraryUser> libraryUsers;
    ArrayList<LibraryObject_LibraryLoan> libraryLoans;
    private String userName;
    private String passWord;
    LibraryObject_ object;
    LibraryEventsInterface libEvents;


public LibrarySession_(Prompt prompt,
            ArrayList<LibraryObject_LibraryBook> books,
            ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            LibraryEventsInterface libEvents)
     {
        this.prompt = prompt;
        this.books = books;
        this.libraryUsers = libraryUsers;
        this.libraryLoans = libraryLoans;
        this.libEvents =  libEvents;
}

    
    public abstract void start();
    
    public boolean login(){
        
        userName = prompt.ask("Ange användar namn >");   
        //passWord = prompt.readPassword("Ange lösenord >");
        passWord = prompt.ask("Ange lösenord >");
     
        return true;
    }
}


