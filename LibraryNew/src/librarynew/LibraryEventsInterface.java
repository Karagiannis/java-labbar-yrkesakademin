/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

/**
 *
 * @author Lasse
 */
public interface LibraryEventsInterface {

    int WelcomeString();

    /*******************Registration       book dat
     * @return a**************************/
    LibraryObject_LibraryBook registerABook();
    //LibraryObject_ registerABook();
    
    LibraryObject_LibraryBook deRegisterABook();

    /******************** Registation        bookloan
     * @return  *************************/
    LibraryObject_LibraryLoan registerALoan();
    LibraryObject_LibraryLoan deRegisterALoan();
    /****************************************************************/
    /************************* Registration events******************/
 

    /****************************  Registration  user data******************************/
    LibraryObject_LibraryUser registerAUser();
    
    LibraryObject_LibraryUser registerAUserFromGUI(String name, String familyName, String personNumber,
            String postAdress, String postNumber, String city, String email);
    
    LibraryObject_LibraryUser deRegisterAUser();

    /**********************************************************************/
    /***********************Searching**************************************/
    void search();
    
    void searchAuthorData();

    /****************Searching        LibraryObject_LibraryBook related**************************/
    void searchTitleData();

    int findBookIDFromTitle(String title);

    boolean extractBookDataFrombookID(int id);

    /**************Searching       User data related**************************/
    void searchUserData();

    int findUserIDFromName(String fullName);

    boolean extractUserDataFromID(int id);
    
    boolean quitProgram();
    int findLoginData(String fullName, String PassWord);

    
    
}
