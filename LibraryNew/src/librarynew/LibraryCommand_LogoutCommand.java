/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LibraryCommand_LogoutCommand extends LibraryCommand_{
    
    public LibraryCommand_LogoutCommand(ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            ArrayList<LibraryObject_LibraryBook> libraryBooks,
            Prompt prompt,
            LibraryEventsInterface libEvents){
            super(libraryUsers, libraryLoans,libraryBooks,prompt, libEvents);
    
    }

    @Override
    protected String getCommand() {
        return "Logga ut";
    }

    @Override
    protected LibraryObject_ onExecute() {
        return null; //To change body of generated methods, choose Tools | Templates.
    }
    
}
