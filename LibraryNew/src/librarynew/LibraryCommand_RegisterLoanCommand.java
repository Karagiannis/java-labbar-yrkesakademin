/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author Lasse
 */
public class LibraryCommand_RegisterLoanCommand extends LibraryCommand_{
        
        
    
        public LibraryCommand_RegisterLoanCommand(ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            ArrayList<LibraryObject_LibraryBook> libraryBooks,
            Prompt prompt,
            LibraryEventsInterface libEvents){
            super(libraryUsers, libraryLoans,libraryBooks,prompt, libEvents);
        
            
        }
        
        @Override
        public  String getCommand(){
            return "Registrera lån";
        }    
    
        @Override
        protected  LibraryObject_LibraryLoan onExecute(){
            
            return libEvents.registerALoan();
        
//        LibraryObject_LibraryLoan loan = null;
//        String userID = prompt.ask("Låntagarens ID: ");
//        int iUserID = Integer.parseInt(userID);
//        System.out.println(String.format("%d",iUserID ));
//        
//        String bookID = prompt.ask("Bokens ID: ");
//        int ibookID  = Integer.parseInt(bookID);
//        System.out.println(String.format("Boken som söks har ID %d",ibookID ));
//        
//        for(LibraryObject_LibraryUser libraryUser : libraryUsers){
//            if(iUserID == libraryUser.userID)
//            {
//                System.out.println("Användaren finns i databasen");
//                for(LibraryObject_LibraryBook book : libraryBooks){
//                    System.out.println(String.format("Bokens som söks är %d, hittar %d",ibookID, book.getBookID() ));
//                    if (book.getBookID() == ibookID)
//                    {   System.out.println("Boken existerar");
//                        if(book.isAvailibe()){
//                            System.out.println("Boken finns tillgänglig");
//                            //libraryUser.registerALoan(book);
//                            //String loanExpires = book.loan();
//                            
//                            book.setAsUnAvailible();
//                            System.out.println("Lånet har registrerats");
//                            //System.out.println(loanExpires);
//                            loan = new LibraryObject_LibraryLoan();
//                            loan.bookID = book.getBookID();
//                            loan.loanID = (int) (libraryLoans.size() + Math.floor( Math.random() * 11111111.1 ));
//                            loan.loanDate = getTodaysDate();
//                            book.wasLoanedLastTimeAt = loan.loanDate;
//                            loan.expiresDate = getExpirationDate();
//                            book.loanExpires = loan.expiresDate;
//                            loan.userID = iUserID;
//                            libraryLoans.add(loan);
//                            return loan;
//                        }
//                        else{
//                            System.out.println("Boken är inte tillgänglig");
//                            break;
//                        }
//                    }else{System.out.println("Söker boken/mediat");}
//                }
//             break;
//            } else System.out.println("Söker användaren...");
//        }//end for
//        return loan;
            
        }   
       
        
}
