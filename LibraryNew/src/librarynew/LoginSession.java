/*
 * Yrkesakademin Ostersund
 * Lärare: Fredrik Håkansson
 * 
 * Laboration: xx
 * @Author :Lasse Karagiannis
 * Terminn: 2015
 * Filnamn: .java
 */
package librarynew;

import java.util.ArrayList;

/**
 *
 * @author Lasse
 */
public class LoginSession {
    Prompt prompt;
    ArrayList<LibraryObject_LibraryBook> books;
    ArrayList<LibraryObject_LibraryUser> libraryUsers;
    ArrayList<LibraryObject_LibraryLoan> libraryLoans;
    private String userName;
    private String passWord;
    LibraryObject_ object;
    LibraryEventsInterface libEvents;

public LoginSession(Prompt prompt,
            ArrayList<LibraryObject_LibraryBook> books,
            ArrayList<LibraryObject_LibraryUser> libraryUsers,
            ArrayList<LibraryObject_LibraryLoan> libraryLoans,
            LibraryEventsInterface libEvents)
     {
        this.prompt = prompt;
        this.books = books;
        this.libraryUsers = libraryUsers;
        this.libraryLoans = libraryLoans;
        this.libEvents =  libEvents;



}
    
}
